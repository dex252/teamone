﻿namespace model_TeamOne
{
    public class ResponseObject<T>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public int Count { get; set; }
        public IList<T> Items { get; set; }
    }
}
