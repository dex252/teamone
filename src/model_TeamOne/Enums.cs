﻿using System.ComponentModel.DataAnnotations;

namespace model_TeamOne
{
    public enum Condition
    {
        Fail,
        Done,
        IsBusy
    }
    /// <summary>
    /// Определение поведение транзакции (счёта)
    /// </summary>
    public enum TransactionOperation
    {
        /// <summary>
        /// Зачисление средств (на счёт пользователю)
        /// </summary>
        Transfer,
        /// <summary>
        /// Снятие средств (со счёта пользователя)
        /// </summary>
        Withdraw,
    }
    public enum PaymentOperation
    {
        None,
        /// <summary>
        /// Продажа
        /// </summary>
        Sale,
        /// <summary>
        /// Покупка
        /// </summary>
        Buy,
        /// <summary>
        /// Зачисление средств
        /// </summary>
        Transfer,
        /// <summary>
        /// Снятие средств
        /// </summary>
        Withdraw,
        /// <summary>
        /// Конвертация с одной валюты в другу
        /// </summary>
        Convertation,
    }
    /// <summary>
    /// Определени какой величины выводить график
    /// </summary>
    public enum DateSlicer
    {
        /// <summary>
        /// В течении 30 дней по дню
        /// </summary>
        [Display(Name = "за месяц")]
        DayToMonth,
        /// <summary>
        /// В течении года по месяцу
        /// </summary>
        [Display(Name = "за год")]
        MonthToYear
    }
    public enum TypeMethod
    {
        None,
        Get,
        Post,
        Put,
        Delete
    }
}
