﻿namespace model_TeamOne.Model.Auth
{
    public class LoginResponse
    {
        public string? Token { get; set; }
        public Guid? RefreshToken { get; set; }
    }
}
