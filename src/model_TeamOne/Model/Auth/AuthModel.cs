﻿using System.ComponentModel.DataAnnotations;

namespace model_TeamOne.Model.Auth
{
    public class AuthModel
    {
        [Required(ErrorMessage = "Логин обязятелен")]
        [MinLength(3, ErrorMessage = "Логин не может быть меньше трёх символов")]
        public string? Login { get; set; }
        [Required(ErrorMessage = "Пароль обязателен")]
        [MinLength(3, ErrorMessage = "Пароль не может быть меньше трёх символов")]
        public string? Password { get; set; }
    }
    public class RegistrationModel : AuthModel
    {
        [Required(ErrorMessage = "Повторение пароля обязательно")]
        [Compare("Password", ErrorMessage = "Пароли должны совпадать")]
        public string? ConfirmPassword { get; set; }
    }
    public class ChangeModel
    {
        [MinLength(3, ErrorMessage = "Имя пользователя не может быть меньше трёх символов")]
        public string Name { get; set; }
    }
}
