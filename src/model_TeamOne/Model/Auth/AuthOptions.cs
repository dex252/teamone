﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace model_TeamOne.Model.Auth
{
    public class AuthOptions
    {
        public const string ISSUER = "TeamOne"; // издатель токена
        public const string AUDIENCE = "www.TeamOne.ru"; // потребитель токена
        public const string KEY = "kjashdflj1238tgsbdnflksuckjhgvfsdfasasdjd";   // ключ для шифрации (может быть любой)
        public static SymmetricSecurityKey GetSymmetricSecurityKey() =>
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(KEY));
    }
}
