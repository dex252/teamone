﻿namespace model_TeamOne.Model
{
    public class PaymentOrderPrint
    {
        /// <summary>
        /// Id Документа (внутрений) (нужно указать в углу документа)
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Номер документа
        /// </summary>
        public long NumberId { get; set; }
        /// <summary>
        /// ФИО клиента
        /// </summary>
        public string Client { get; set; }
        /// <summary>
        /// Счёт клиента
        /// </summary>
        public long ClientBank { get; set; }
        /// <summary>
        /// Тип операции
        /// </summary>
        public PaymentOperation TypeOperation { get; set; }
        /// <summary>
        /// Состояние выполнения
        /// </summary>
        public Condition Condition { get; set; }
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string MessageError { get; set; } = string.Empty;
        /// <summary>
        /// Имя компании
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// Счёт компании
        /// </summary>
        public long CompanyBank { get; set; }
        /// <summary>
        /// Количество акций (если была проведена сделка покупки акций)
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// Сумма сделки
        /// </summary>
        public double Sum { get; set; }
        /// <summary>
        /// Наименование валюты
        /// </summary>
        public string Valute { get; set; }
        /// <summary>
        /// Сумма сделки 2 (если была конвертация)
        /// </summary>
        public double Sum2 { get; set; }
        /// <summary>
        /// Наименование валюты 2 (если была конвертация)
        /// </summary>
        public string Valute2 { get; set; }
        /// <summary>
        /// Дата проведения документа
        /// </summary>
        public DateTime DateOrder { get; set; }

        /// <summary>
        /// Набор изображений в формате base64
        /// </summary>
        public Dictionary<string, string> ImagesBase64 { get; set; }

        public string TypeOperationString => TypeOperation switch
        {
            PaymentOperation.Buy => "Покупка акций",
            PaymentOperation.Sale => "Продажа акций",
            PaymentOperation.Convertation => "Конвертация средств",
            PaymentOperation.Transfer => "Зачисления средств",
            PaymentOperation.Withdraw => "Вывод средств",
            _ => "Другое"
        };
        public string ConditionString => Condition switch
        {
            Condition.IsBusy => "В процессе выполнения",
            Condition.Fail => "Ошибка выполнения",
            Condition.Done => "Выполнено",
            _ => "Другое"
        };
    }
}
