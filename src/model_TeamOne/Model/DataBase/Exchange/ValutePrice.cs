﻿namespace model_TeamOne.Model.DataBase.Exchange
{
    /// <summary>
    /// История изменения цены на валюту
    /// </summary>
    public class ValutePrice : Table
    {
        public Guid ValuteId { get; set; }
        /// <summary>
        /// Будет разумно записывать 1 дату в день. Не более
        /// </summary>
        public DateTime DateHistory { get; set; }
        /// <summary>
        /// Коэфициент покупки
        /// Используется при конвертации из рублей в эту валюту
        /// </summary>
        public double KoefBuy { get; set; }
        /// <summary>
        /// Коэфициент продажи
        /// Используется при конвертации этой валюты в рубли
        /// </summary>
        public double KoefSell { get; set; }
    }
}
