﻿using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase.Exchange
{
    public class CompanyQuarterFinance : Table
    {
        public Guid CompanyId { get; set; }
        public int Year { get; set; }
        public int Quarter { get; set; }
        public double Value { get; set; }
        [NotMapped] public string QuarterString => $"Квартал {Quarter}";
        [NotMapped] public int ValueInt => (int)Value;
    }
}
