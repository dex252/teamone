﻿using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase.Exchange
{
    /// <summary>
    /// Валюта.
    /// Имеет основные типы валют на "бирже" и их коэфициент относительно рубля.
    /// </summary>
    public class Valute : Table
    {
        /// <summary>
        /// Наименование - напимер Рубль
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Символ - например ₽
        /// </summary>
        public string Symbol { get; set; }
        [NotMapped] 
        public double KoefBuy { get; set; }
        [NotMapped]
        public double KoefSell { get; set; }
        [NotMapped]
        public double KoefBuyLastDay { get; set; }
        [NotMapped]
        public double KoefSellLastDay { get; set; }
    }
}
