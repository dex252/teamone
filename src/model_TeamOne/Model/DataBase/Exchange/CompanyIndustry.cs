﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model_TeamOne.Model.DataBase.Exchange
{
    public class CompanyIndustry : Table
    {
        public Guid CompanyId { get; set; }
        public Guid IndustryId { get; set; }
    }
}
