﻿using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase.Exchange
{
    /// <summary>
    /// История стоимости акций компании
    /// </summary>
    public class CompanyStockPrice : Table
    {
        public Guid CompanyId { get; set; }
        public DateTime DateHistory { get; set; }
        public double Price { get; set; }
        public double PriceMax { get; set; }
        public double PriceMin { get; set; }
        public double PriceOpen { get; set; }
        public double PriceClose { get; set; }
        [NotMapped] public string PeriodFormat { get; set; }
        [NotMapped] public string PeriodString => DateHistory.ToString(this.PeriodFormat);
    }
}
