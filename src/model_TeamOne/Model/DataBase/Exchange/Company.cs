﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase.Exchange
{
    /// <summary>
    /// Компания
    /// </summary>
    public class Company : Table
    {
        /// <summary>
        /// Сокращённое название для бирж
        /// </summary>
        [Required(ErrorMessage = "Обязательно поле")]
        [MinLength(3, ErrorMessage = "Не меньше трёх символов")]
        public string Title { get; set; }
        /// <summary>
        /// Полное наименование
        /// </summary>
        [Required(ErrorMessage = "Обязательно поле")]
        [MinLength(3, ErrorMessage = "Не меньше трёх символов")]
        public string Name { get; set; }
        /// <summary>
        /// Путь к аватару
        /// </summary>
        public string Avatar { get; set; }
        /// <summary>
        /// Адрес
        /// </summary>
        public string Adress { get; set; }
        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Валюта в которой изначально продаются акции компании
        /// </summary>
        public Guid ValuteId { get; set; }
        /// <summary>
        /// Страна компании
        /// </summary>
        public Guid CountryId { get; set; }
        /// <summary>
        /// Если это компания для валюты
        /// </summary>
        public bool IsValute { get; set; }
        /// <summary>
        /// Количество акций в 1 покупке
        /// </summary>
        public int Package { get; set; }
        [NotMapped] public List<CompanyStockPrice> StockPrice { get; set; }
        [NotMapped] public List<Guid> Industries { get; set; }
        [NotMapped] public (double?, double?) LastPrice => (StockPrice is null ? (0,0) : (StockPrice.TakeLast(2).FirstOrDefault()?.Price, StockPrice.TakeLast(2).LastOrDefault()?.Price));
        [NotMapped] public double? Spent => (StockPrice is null ? 0 : LastPrice.Item2 - LastPrice.Item1);
        [NotMapped] public double? Delta => (StockPrice is null ? 0 : LastPrice.Item2 / LastPrice.Item1 - 1);
    }
}
