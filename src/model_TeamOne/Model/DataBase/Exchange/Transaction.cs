﻿using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase.Exchange
{
    public class Transaction : Table
    {
        public Guid UserId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid PaymentOrderId { get; set; }
        public DateTime DateHistory { get; set; }
        public TransactionOperation TypeOperation { get; set; }
        public double Sum { get; set; }
    }
}
