﻿using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase.Exchange
{
    public class Balance : Table
    {
        public Guid UserId { get; set; }
        public Guid CompanyId { get; set; }
        public double Value { get; set; }
        [NotMapped] public double SumBuy { get; set; }
        [NotMapped] public double SumSale { get; set; }
        [NotMapped] public double SumDelta => SumSale / SumBuy - 1;
        [NotMapped] public Company Company { get; set; }
        [NotMapped] public bool IsValuteAll { get; set; }

    }
}
