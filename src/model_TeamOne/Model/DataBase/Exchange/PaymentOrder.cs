﻿using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase.Exchange
{
    /// <summary>
    /// Платёжное поручение
    /// </summary>
    public class PaymentOrder : Table
    {
        public Guid UserId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CompanyId2 { get; set; }
        public DateTime DateHistory { get; set; }
        public PaymentOperation TypeOperation { get; set; }
        public string MessageError { get; set; } = string.Empty;
        public Condition Condition { get; set; }
        public double Sum { get; set; }
        public double Sum2 { get; set; }
        public int Count { get; set; }
        [NotMapped]
        public string TypeOperationString => TypeOperation switch
        {
            PaymentOperation.Buy => "Покупка",
            PaymentOperation.Sale => "Продажа",
            PaymentOperation.Convertation => "Конвертация",
            PaymentOperation.Transfer => "Зачисления средств",
            PaymentOperation.Withdraw => "Вывод средств",
            _ => "Другое"
        };
        [NotMapped]
        public string ConditionString => Condition switch
        {
            Condition.IsBusy => "В процессе выполнения",
            Condition.Fail => "Ошибка выполнения",
            Condition.Done => "Выполнено",
            _ => "Другое"
        };
    }
}
