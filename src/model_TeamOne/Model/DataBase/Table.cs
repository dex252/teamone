﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase
{
    public class Table
    {
        /// <summary>
        /// Ключ, будет у каждой таблиц
        /// </summary>
        [Key]
        public Guid Id { get; set; }
        /// <summary>
        /// Дата создания записи
        /// </summary>
        public DateTime etl_date { get; set; }
        /// <summary>
        /// Признак, удаления записи
        /// </summary>
        public bool IsDelete { get; set; }
        public Table()
        {
            Id = Guid.NewGuid();
            etl_date = DateTime.Now;
            IsDelete = false;
        }
        [NotMapped] public long NumberId => Math.Abs(BitConverter.ToInt64(Id.ToByteArray(), 0));
    }
}
