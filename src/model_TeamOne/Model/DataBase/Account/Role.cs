﻿namespace model_TeamOne.Model.DataBase.Account
{
    public class Role : Table
    {
        public string Name { get; set; }
        public int Priority { get; set; }
    }
}
