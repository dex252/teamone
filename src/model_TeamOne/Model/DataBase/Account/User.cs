﻿using System.ComponentModel.DataAnnotations.Schema;

namespace model_TeamOne.Model.DataBase.Account
{
    public class User : Table
    {
        public string? Login { get; set; }
        public string? Name { get; set; }
        public string? Password { get; set; }
        public string? Theme { get; set; }

        [NotMapped]
        public List<Role>? Roles { get; set; }
    }
}
