﻿namespace model_TeamOne.Model.DataBase.Account
{
    public class UserRole : Table
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
    }
}
