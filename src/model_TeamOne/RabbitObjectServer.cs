﻿namespace model_TeamOne
{
    public class RabbitObjectServer
    {
        public Guid Id { get; set; }
        public bool IsComplite { get; set; }
        public string Message { get; set; } = string.Empty;
    }
}
