﻿namespace model_TeamOne
{
    public class RabbitObjectUser
    {
        public Guid UserId { get; set; }
        public Guid OrderId { get; set; }
    }
}
