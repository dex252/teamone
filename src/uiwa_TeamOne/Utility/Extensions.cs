﻿using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using System.Web;

namespace uiwa_TeamOne.Utility
{
    public static class Extensions
    {
        public static Uri GetFiltersQuery(this Uri uri, Dictionary<string, object> values)
        {
            if (values.Count == 0) return uri;

            UriBuilder uriBuilder = new UriBuilder(uri);
            NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(uriBuilder.Query);
            values.ToList().ForEach(value =>
            {
                DefaultInterpolatedStringHandler defaultInterpolatedStringHandler = new DefaultInterpolatedStringHandler(0, 1);
                defaultInterpolatedStringHandler.AppendFormatted(value.Value);
                nameValueCollection[$"{value.Key}"] = defaultInterpolatedStringHandler.ToStringAndClear();
            });

            uriBuilder.Query = nameValueCollection.ToString();
            return uriBuilder.Uri;
        }

    }
}
