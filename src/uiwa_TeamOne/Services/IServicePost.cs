﻿namespace uiwa_TeamOne.Services
{
    public interface IServicePost<T>
    {
        Task<T> Post(T model);
    }
}
