﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.SignalR.Client;
using model_TeamOne;
using System.Security.Claims;

namespace uiwa_TeamOne.Services.Hubs
{
    public class HubNotification
    {
        private readonly ILocalStorageService _localStorageService;
        private Func<Guid, string, Task> _handleChange;
        private HubConnection _hubConnection;
        private Guid? _id;
        private bool _isConnection;

        public HubNotification(ILocalStorageService localStorageService)
        {
            _localStorageService = localStorageService;
        }
        public async Task Start(Func<Guid, string, Task> handleChange)
        {
            _handleChange = null;
            _handleChange += handleChange;

            if (!_isConnection)
            {
                if (_id is null)
                {
                    string _token = await _localStorageService.GetItemAsStringAsync("token");
                    var jwt = JwtParser.ParseClaimsFromJwt(_token);
                    _id = Guid.Parse(jwt.First(s => s.Type == ClaimTypes.NameIdentifier).Value);
                }
                _hubConnection = new HubConnectionBuilder().WithUrl(Setting.BASIC_ADRESS_NOTIFICATION_HUB).Build();
                _hubConnection.On<string, string>(_id.ToString(), async (orderId, message) =>
                {
                    await _handleChange?.Invoke(Guid.Parse(orderId), message);
                });
                await _hubConnection.StartAsync();
                _isConnection = true;
            }
        }
    }
}
