﻿using Blazored.LocalStorage;
using System.Net.Http.Headers;

namespace uiwa_TeamOne.Services
{
    public abstract class ServiceAbstract
    {
        protected readonly ILocalStorageService _localStorageService;
        protected readonly HttpClient _httpClient;
        public ServiceAbstract(ILocalStorageService localStorageService, HttpClient httpClient)
        {
            _localStorageService = localStorageService;
            _httpClient = httpClient;
        }
        protected virtual async Task DefaultHeaders()
        {
            try
            {
                string _token = await _localStorageService.GetItemAsStringAsync("token");
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token.Trim('"'));
            } 
            catch (Exception ex)
            {
                _httpClient.DefaultRequestHeaders.Clear();
            }
        }
    }
}
