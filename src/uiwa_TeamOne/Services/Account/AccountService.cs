﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using model_TeamOne.Model.Auth;
using model_TeamOne;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Reflection;

namespace uiwa_TeamOne.Services.Account
{
    public class AccountService : IAccountService
    {
        private readonly ILocalStorageService _localStorageService;
        private readonly AuthenticationStateProvider _customAuthenticationProvider;
        private readonly HttpClient _httpClient;
        public AccountService(ILocalStorageService localStorageService,
                              AuthenticationStateProvider customAuthenticationProvider,
                              HttpClient httpClient)
        {
            _localStorageService = localStorageService;
            _customAuthenticationProvider = customAuthenticationProvider;
            _httpClient = httpClient;
        }
        private async Task DefaultHeaders()
        {
            string _token = await _localStorageService.GetItemAsStringAsync("token");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token.Trim('"'));
        }
        public async Task<bool> LoginAsync(AuthModel model)
        {
            try
            {
                model.Password = model.Password;
                var response = await _httpClient.PostAsJsonAsync($"{Setting.BASIC_ADRESS}/login", model);
                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }
                return await ReturnMarkChangeLogin(response);
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> RegisterAsync(RegistrationModel model)
        {
            try
            {
                model.Password = model.Password;
                model.ConfirmPassword = model.ConfirmPassword;
                var response = await _httpClient.PostAsJsonAsync($"{Setting.BASIC_ADRESS}/registration", model);
                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }
                return await ReturnMarkChangeLogin(response);
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> LogoutAsync()
        {
            await _localStorageService.RemoveItemAsync("token");
            await _localStorageService.RemoveItemAsync("id");
            ((CustomAuthenticationProvider)_customAuthenticationProvider).Notify();
            return true;
        }
        private async Task<bool> ReturnMarkChangeLogin(HttpResponseMessage response)
        {
            LoginResponse? authData = await response.Content.ReadFromJsonAsync<LoginResponse>();
            if (authData != null)
            {
                await _localStorageService.SetItemAsync("token", authData.Token);
                await _localStorageService.SetItemAsync("id", authData.RefreshToken);
                ((CustomAuthenticationProvider)_customAuthenticationProvider).Notify();
                return true;
            }
            return false;
        }
        private async Task<bool> UpdateToken()
        {
            try
            {
                var authData = await _httpClient.GetFromJsonAsync<LoginResponse>($"{Setting.BASIC_ADRESS}/updatetoken"); 
                if (authData != null)
                {
                    await _localStorageService.SetItemAsync("token", authData.Token);
                    await _localStorageService.SetItemAsync("id", authData.RefreshToken);
                    ((CustomAuthenticationProvider)_customAuthenticationProvider).Notify();
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> ChangeTheme(string theme)
        {
            try
            {
                await DefaultHeaders();
                var result = await _httpClient.GetFromJsonAsync<bool>($"{Setting.BASIC_ADRESS}/updatetheme/{theme}");
                if (!result)
                {
                    return false;
                }

                return await UpdateToken();
            }
            catch
            {
                return false;
            }
        }
    }
}
