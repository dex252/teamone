﻿using model_TeamOne.Model.Auth;

namespace uiwa_TeamOne.Services.Account
{
    public interface IAccountService
    {
        Task<bool> LoginAsync(AuthModel model);
        Task<bool> RegisterAsync(RegistrationModel model);
        Task<bool> LogoutAsync();
        Task<bool> ChangeTheme(string theme);
    }
}
