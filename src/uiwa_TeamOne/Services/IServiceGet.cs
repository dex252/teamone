﻿namespace uiwa_TeamOne.Services
{
    public interface IServiceGet<T>
    {
        Task<IList<T>> Get(Dictionary<string, object> parameters = null);
        Task<T> GetById(Guid id);
        Task<int> GetCount(Dictionary<string, object> parameters = null);
    }
}
