﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;
using uiwa_TeamOne.Utility;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceTransaction : ServiceAbstract, IServiceGet<Transaction>
    {
        public ServiceTransaction(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<Transaction>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                if (parameters is null) 
                { 
                    return new List<Transaction>(); 
                }
                await DefaultHeaders();
                Uri uri = new Uri($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_TRANSACTION}").GetFiltersQuery(parameters);
                return await _httpClient.GetFromJsonAsync<List<Transaction>>(uri);
            }
            catch
            {
                return new List<Transaction>();
            }
        }
        public async Task<Transaction> GetById(Guid id)
        {
            return new Transaction();
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            return 0;
        }
    }
}
