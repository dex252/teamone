﻿using Blazored.LocalStorage;
using model_TeamOne.Model.DataBase.Exchange;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceCompanyQuarterFinance : ServiceAbstract, IServiceGet<CompanyQuarterFinance>
    {
        public ServiceCompanyQuarterFinance(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<CompanyQuarterFinance>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                Random rnd = new Random();
                var value = rnd.Next(10000, 10000000);
                Guid.TryParse(parameters["CompanyId"].ToString(), out var companyId);
                int.TryParse(parameters["Year"].ToString(), out var Year);
                return new List<CompanyQuarterFinance>
                {
                    new CompanyQuarterFinance { CompanyId = companyId, Year = Year, Quarter = 1, Value = value + 10000 * rnd.NextDouble() },
                    new CompanyQuarterFinance { CompanyId = companyId, Year = Year, Quarter = 2, Value = value + 10000 * rnd.NextDouble() },
                    new CompanyQuarterFinance { CompanyId = companyId, Year = Year, Quarter = 3, Value = value + 10000 * rnd.NextDouble() },
                    new CompanyQuarterFinance { CompanyId = companyId, Year = Year, Quarter = 4, Value = value + 10000 * rnd.NextDouble() }
                };
            }
            catch
            {
                return new List<CompanyQuarterFinance>();
            }
        }
        public async Task<CompanyQuarterFinance> GetById(Guid id)
        {
            return null;
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            return 0;
        }
    }
}
