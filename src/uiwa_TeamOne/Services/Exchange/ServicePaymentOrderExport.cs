﻿using Microsoft.AspNetCore.Components;
using model_TeamOne;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServicePaymentOrderExport
    {
        private NavigationManager _navigationManager;
        public ServicePaymentOrderExport(NavigationManager navigationManager)
        {
            _navigationManager = navigationManager;
        }
        public void Export(Guid id)
        {
            _navigationManager.NavigateTo($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_PRINT_PAYMENT_ORDER}/{id}");
        }
    }
}
