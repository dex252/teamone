﻿using Blazored.LocalStorage;
using model_TeamOne;
using System.Net.Http.Json;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceTestCondition : ServiceAbstract
    {
        public ServiceTestCondition(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<bool> Post()
        {
            try
            {
                await DefaultHeaders();
                var response = await _httpClient.PostAsJsonAsync($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_TESTCONDOTION}", true);
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> PostUserBalance()
        {
            try
            {
                await DefaultHeaders();
                var response = await _httpClient.PostAsJsonAsync($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_TESTCONDOTION}/UserBalance", true);
                return response.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
        public async Task<int> GetCount(object[] agrs = null)
        {
            return 0;
        }
    }
}
