﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;
using uiwa_TeamOne.Utility;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServicePaymentOrder : ServiceAbstract, IServiceGet<PaymentOrder>, IServicePost<PaymentOrder>
    {
        public ServicePaymentOrder(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<PaymentOrder>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                Uri uri = new Uri($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_PAYMENT_ORDER}").GetFiltersQuery(parameters);
                return await _httpClient.GetFromJsonAsync<List<PaymentOrder>>(uri);
            }
            catch
            {
                return new List<PaymentOrder>();
            }
        }
        public async Task<PaymentOrder> GetById(Guid id)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<PaymentOrder>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_PAYMENT_ORDER}/{id}");
            }
            catch
            {
                return null;
            }
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<int>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_PAYMENT_ORDER}/Count");
            }
            catch
            {
                return 0;
            }
        }

        public async Task<PaymentOrder> Post(PaymentOrder model)
        {
            try
            {
                if (model is null) return null;
                await DefaultHeaders();
                model.DateHistory = DateTime.Now;
                var response = await _httpClient.PostAsJsonAsync($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_PAYMENT_ORDER}", model);
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return await response.Content.ReadFromJsonAsync<PaymentOrder>();
            }
            catch
            {
                return null;
            }
        }
    }
}
