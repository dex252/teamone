﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;
using uiwa_TeamOne.Utility;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServicePaymentOrderCompany : ServiceAbstract, IServiceGet<PaymentOrder>
    {
        public ServicePaymentOrderCompany(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<PaymentOrder>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                Uri uri = new Uri($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_PAYMENT_ORDER_COMPANY}").GetFiltersQuery(parameters);
                return await _httpClient.GetFromJsonAsync<List<PaymentOrder>>(uri);
            }
            catch
            {
                return new List<PaymentOrder>();
            }
        }
        public async Task<PaymentOrder> GetById(Guid id)
        {
            return new PaymentOrder();
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                Uri uri = new Uri($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_PAYMENT_ORDER_COMPANY}/Count").GetFiltersQuery(parameters);
                return await _httpClient.GetFromJsonAsync<int>(uri);
            }
            catch
            {
                return 0;
            }
        }
    }
}
