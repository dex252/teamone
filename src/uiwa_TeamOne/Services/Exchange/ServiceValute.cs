﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceValute : ServiceAbstract, IServiceGet<Valute>
    {
        public ServiceValute(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<Valute>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<List<Valute>>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_VALUTE}");
            }
            catch
            {
                return new List<Valute>();
            }
        }
        public async Task<Valute> GetById(Guid id)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<Valute>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_VALUTE}/{id}");
            }
            catch
            {
                return new Valute();
            }
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            return 0;
        }
    }
}
