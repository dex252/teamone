﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceCountry : ServiceAbstract, IServiceGet<Country>
    {
        public ServiceCountry(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<Country>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<List<Country>>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_COUNTRY}");
            }
            catch
            {
                return new List<Country>();
            }
        }
        public async Task<Country> GetById(Guid id)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<Country>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_COUNTRY}/{id}");
            }
            catch
            {
                return new Country();
            }
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            return 0;
        }
    }
}
