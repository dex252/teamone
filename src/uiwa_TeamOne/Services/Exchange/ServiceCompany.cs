﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceCompany : ServiceAbstract, IServiceGet<Company>
    {
        public ServiceCompany(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<Company>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<List<Company>>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_COMPANY}");
            }
            catch
            {
                return new List<Company>();
            }
        }
        public async Task<Company> GetById(Guid id)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<Company>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_COMPANY}/{id}");
            }
            catch
            {
                return new Company();
            }
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            return 0;
        }
    }
}
