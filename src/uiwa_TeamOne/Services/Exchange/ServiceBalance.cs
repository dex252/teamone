﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceBalance : ServiceAbstract, IServiceGet<Balance>
    {
        public ServiceBalance(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }
        public async Task<IList<Balance>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<IList<Balance>>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_BALANCE}");
            }
            catch
            {
                return new List<Balance>();
            }
        }
        public async Task<Balance> GetById(Guid id)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<Balance>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_BALANCE}/{id}");
            }
            catch
            {
                return new Balance();
            }
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            return 0;
        }
    }
}
