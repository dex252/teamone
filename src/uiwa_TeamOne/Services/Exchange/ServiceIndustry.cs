﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceIndustry : ServiceAbstract, IServiceGet<Industry>
    {
        public ServiceIndustry(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<Industry>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<List<Industry>>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_INDUSTRY}");
            }
            catch
            {
                return new List<Industry>();
            }
        }
        public async Task<Industry> GetById(Guid id)
        {
            try
            {
                await DefaultHeaders();
                return await _httpClient.GetFromJsonAsync<Industry>($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_INDUSTRY}/{id}");
            }
            catch
            {
                return new Industry();
            }
        }
        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            return 0;
        }
    }
}
