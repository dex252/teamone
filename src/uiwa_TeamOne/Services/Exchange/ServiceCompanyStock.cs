﻿using Blazored.LocalStorage;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Net.Http.Json;
using uiwa_TeamOne.Utility;

namespace uiwa_TeamOne.Services.Exchange
{
    public class ServiceCompanyStock : ServiceAbstract, IServiceGet<CompanyStockPrice>
    {
        public ServiceCompanyStock(ILocalStorageService localStorageService, HttpClient httpClient) : base(localStorageService, httpClient)
        {
        }

        public async Task<IList<CompanyStockPrice>> Get(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                Uri uri = new Uri($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_COMPANY_STOCK}").GetFiltersQuery(parameters);
                return await _httpClient.GetFromJsonAsync<List<CompanyStockPrice>>(uri);
            }
            catch
            {
                return new List<CompanyStockPrice>();
            }
        }

        public async Task<CompanyStockPrice> GetById(Guid id)
        {
            return null;
        }

        public async Task<int> GetCount(Dictionary<string, object> parameters = null)
        {
            try
            {
                await DefaultHeaders();
                Uri uri = new Uri($"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_COMPANY_STOCK}/Count").GetFiltersQuery(parameters);
                return await _httpClient.GetFromJsonAsync<int>(uri);
            }
            catch
            {
                return 0;
            }
        }
    }
}
