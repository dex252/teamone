﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using model_TeamOne;
using System.Security.Claims;

namespace uiwa_TeamOne.Services
{
    //AuthenticationStateProvider - стандартный контракт управления аутентификацией
    //Для того что бы использовать свою (JWT Token или другую) нам нужен свой контракт
    public class CustomAuthenticationProvider : AuthenticationStateProvider
    {
        //Лоакльное хранилище, где мы будем хранить токен (в браузере)
        private readonly ILocalStorageService _localStorageService;
        public CustomAuthenticationProvider(ILocalStorageService localStorageService)
        {
            _localStorageService = localStorageService;
        }
        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            string token = await _localStorageService.GetItemAsync<string>("token");
            if (string.IsNullOrEmpty(token))
            {
                var anonymous = new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity() { }));
                return anonymous;
            }
            var userClaimPrincipal = new ClaimsPrincipal(new ClaimsIdentity(JwtParser.ParseClaimsFromJwt(token), "Fake Authentication"));
            var loginUser = new AuthenticationState(userClaimPrincipal);
            return loginUser;
        }
        public void Notify()
        {
            NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
        }
    }
}
