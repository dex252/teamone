﻿using Microsoft.AspNetCore.Components;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using Radzen;

namespace uiwa_TeamOne.Pages.Exchange.Companys
{
    public partial class CompanyPaymentOrderList
    {
        [Parameter] public Company Company { get; set; }

        private IEnumerable<Company> _listCompanys;
        private IList<PaymentOrder> _paymentOrder;
        private Dictionary<Guid, Valute> _valute;
        private Balance _balance;
        private Balance _balancePayment;
        private int _count;
        private bool _isLoading;
        private int _take = 5;
        protected override async Task OnInitializedAsync()
        {
            var valute = await _serviceValute.Get();
            _valute = valute.ToDictionary(s => s.Id, s => s);
            _listCompanys = await _serviceCompany.Get();
            _count = await _servicePaymentCompany.GetCount(new Dictionary<string, object>() { { "Id", Company.Id } });
            await LoadData(new LoadDataArgs() { Skip = 0 });
            await UpdateBalance();
            await _hubNotification.Start(UpdateOrderElement);
        }
        private async Task UpdateBalance()
        {
            _balance = await _serviceBalance.GetById(Company.Id);
            _balancePayment = await _serviceBalance.GetById(_listCompanys.FirstOrDefault(s => s.IsValute == true && s.ValuteId == Company.ValuteId).Id);
        }
        private async Task UpdateOrderElement(Guid id, string message)
        {
            var number = BitConverter.ToInt32(id.ToByteArray(), 0);
            if (message == "Complite")
            {
                ShowNotification(new NotificationMessage { 
                    Severity = NotificationSeverity.Success, 
                    Summary = Setting.UI_NOTIFICATION_TITLE_DEFAULT, 
                    Detail = $"ПП №{number} проведено: успешно", 
                    Duration = Setting.UI_NOTIFICATION_DIRATION_DEFAULT });
            }
            else
            {
                ShowNotification(new NotificationMessage { 
                    Severity = NotificationSeverity.Error, 
                    Summary = Setting.UI_NOTIFICATION_TITLE_DEFAULT, 
                    Detail = $"ПП №{number} не проведено: {message}", 
                    Duration = Setting.UI_NOTIFICATION_DIRATION_DEFAULT });
            }
            if (_paymentOrder.Count > 0)
            {
                var result = await _servicePayment.GetById(id);
                if (result is not null)
                {
                    var resetItem = _paymentOrder.FirstOrDefault(s => s.Id == result.Id);
                    if (resetItem != null)
                    {
                        var position = _paymentOrder.IndexOf(resetItem);
                        _paymentOrder.Insert(position, result);
                        _paymentOrder.Remove(resetItem);
                    }
                    await UpdateBalance();
                    await UpdateListPaymentOrder();
                }
            }
        }
        private void ShowNotification(NotificationMessage message)
        {
            _serviceNotification.Notify(message);
        }
        private async void AddOrderElement(PaymentOrder order)
        {
            _count += 1;
            _paymentOrder.Add(order);
            await UpdateListPaymentOrder();
        }
        private async Task LoadData(LoadDataArgs args)
        {
            _isLoading = true;
            _paymentOrder = await _servicePaymentCompany.Get(new Dictionary<string, object>() { { "skip", args.Skip }, { "take", _take }, { "Id", Company.Id } });
            await UpdateListPaymentOrder();
            _isLoading = false;
        }
        private async Task UpdateListPaymentOrder()
        {
            _paymentOrder = _paymentOrder.OrderByDescending(s => s.DateHistory).ToList();
            StateHasChanged();
        }
        public async Task OpenDialog(bool operation)
        {
            var paymentOrder = new PaymentOrder();
            paymentOrder.TypeOperation = operation == true ? PaymentOperation.Buy : PaymentOperation.Sale;
            PaymentOrder order = await _serviceDialog.OpenAsync<CompanyPaymentOrderCard>($"Операция {paymentOrder.TypeOperationString}",
                                           new Dictionary<string, object>() { { "BalancesMy", _balancePayment }, { "BalanceCompany", _balance }, { "PaymentOrders", paymentOrder }, { "Companys", Company }, { "Valutes", _valute[Company.ValuteId] } },
                                           new DialogOptions() { Resizable = false, Draggable = false });

            AddOrderElement(await _servicePaymentPost.Post(order));
        }
        private BadgeStyle GetBangeStyle(double value)
        {
            if (value > 0) return BadgeStyle.Success;
            return BadgeStyle.Danger;
        }
    }
}
