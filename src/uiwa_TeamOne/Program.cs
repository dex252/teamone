using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using model_TeamOne.Model.DataBase.Exchange;
using Radzen;
using uiwa_TeamOne;
using uiwa_TeamOne.Services;
using uiwa_TeamOne.Services.Account;
using uiwa_TeamOne.Services.Exchange;
using uiwa_TeamOne.Services.Hubs;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

builder.Services.AddScoped<AuthenticationStateProvider, CustomAuthenticationProvider>();
builder.Services.AddAuthorizationCore();

builder.Services.AddScoped<DialogService>();
builder.Services.AddScoped<NotificationService>();
builder.Services.AddScoped<TooltipService>();
builder.Services.AddScoped<ContextMenuService>();
//Service Technical
builder.Services.AddScoped<ServiceTestCondition, ServiceTestCondition>();
builder.Services.AddScoped<IAccountService, AccountService>();
//Service Get
builder.Services.AddScoped<IServiceGet<Company>, ServiceCompany>();
builder.Services.AddScoped<IServiceGet<CompanyQuarterFinance>, ServiceCompanyQuarterFinance>();
builder.Services.AddScoped<IServiceGet<Balance>, ServiceBalance>();
builder.Services.AddScoped<IServiceGet<Valute>, ServiceValute>();
builder.Services.AddScoped<IServiceGet<Transaction>, ServiceTransaction>();
builder.Services.AddScoped<IServiceGet<Country>,  ServiceCountry>();
builder.Services.AddScoped<IServiceGet<Industry>, ServiceIndustry>();
builder.Services.AddScoped<IServiceGet<PaymentOrder>, ServicePaymentOrder>();
builder.Services.AddScoped<ServicePaymentOrderCompany, ServicePaymentOrderCompany>();
builder.Services.AddScoped<ServicePaymentOrderExport, ServicePaymentOrderExport>();
builder.Services.AddScoped<IServiceGet<CompanyStockPrice>, ServiceCompanyStock>();
//Service Post
builder.Services.AddScoped<IServicePost<PaymentOrder>, ServicePaymentOrder>();
//ServiceOther
builder.Services.AddScoped<HubNotification>();

builder.Services.AddBlazoredLocalStorage();

await builder.Build().RunAsync();
