﻿using model_TeamOne;

namespace api_TeamOne.Service.Rabbit
{
    public interface IRabbitService
    {
        Task SendMessage(object obj, string queue);
    }
}
