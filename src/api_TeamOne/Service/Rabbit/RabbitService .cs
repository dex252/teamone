﻿using model_TeamOne;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace api_TeamOne.Service.Rabbit
{
    public class RabbitService : IRabbitService
    {
        public async Task SendMessage(object obj, string queue)
        {
            var message = JsonSerializer.Serialize(obj);
            await SendMessage(message, queue);
        }
        private async Task SendMessage(string message, string queue)
        {
            var factory = new ConnectionFactory()
            {
                HostName = Setting.RABBITMQ_HOST,
                UserName = Setting.RABBITMQ_USER,
                Password = Setting.RABBITMQ_PASSWORD
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queue,
                                   durable: false,
                                   exclusive: false,
                                   autoDelete: false,
                                   arguments: null);

                    channel.BasicPublish(exchange: "",
                                   routingKey: queue,
                                   basicProperties: null,
                                   body: Encoding.UTF8.GetBytes(message));
                }
            }
        }
    }
}
