﻿using model_TeamOne;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Text.Json;

namespace api_TeamOne.Service.Rabbit
{
    public abstract class RabbitReaderService : BackgroundService
    {
        //private readonly HttpClient _httpClient;
        //private IServiceProvider _services;
        private IHttpClientFactory _httpClientFactory;
        private IConnection _connection;
        private IModel _channel;
        protected virtual string _queue => "None";
        protected virtual string _path => "None";
        protected virtual TypeMethod _typeMethod => TypeMethod.None;
        public RabbitReaderService(IHttpClientFactory httpClientFactory/*HttpClient httpClient/*, IServiceProvider services*/)
        {
            _httpClientFactory = httpClientFactory;
            //_services = services;
            //_httpClient = httpClient;
            var factory = new ConnectionFactory { HostName = Setting.RABBITMQ_HOST, UserName = Setting.RABBITMQ_USER, Password = Setting.RABBITMQ_PASSWORD };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: _queue, durable: false, exclusive: false, autoDelete: false, arguments: null);
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (ch, ea) =>
            {
                var result = JsonSerializer.Deserialize<RabbitObjectServer>(Encoding.UTF8.GetString(ea.Body.ToArray()));
                await DoWork(stoppingToken, result);
                _channel.BasicAck(ea.DeliveryTag, false);
            };
            _channel.BasicConsume(_queue, false, consumer);

            return Task.CompletedTask;
        }
        protected virtual async Task DoWork(CancellationToken stoppingToken, RabbitObjectServer obj)
        {
            obj.Message = obj.Message ?? "";
            using (var httpClient = _httpClientFactory.CreateClient("_myHttpClient"))
            {
                try
                {
                    switch (_typeMethod)
                    {
                        case TypeMethod.Get: break;
                        case TypeMethod.Post: await httpClient.PostAsJsonAsync(_path, obj, stoppingToken); break;
                        case TypeMethod.Put: await httpClient.PutAsJsonAsync(_path, obj, stoppingToken); break;
                        case TypeMethod.Delete: break;
                        default: break;
                    }
                }
                catch (Exception ex) 
                {
                    var z = ex.ToString();
                }
            }
            //using (var scope = _services.CreateScope())
            //{
            //    var scopedProcessingService = scope.ServiceProvider.GetRequiredService<IScopedProcessingService>();
            //    await scopedProcessingService.DoWork(stoppingToken, obj);
            //}
        }
        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
