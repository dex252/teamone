﻿using api_TeamOne.Service.Rabbit;
using model_TeamOne;

namespace api_TeamOne.Service.Pay
{
    public class PaymentReaderService : RabbitReaderService
    {
        public PaymentReaderService(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }
        protected override string _queue => Setting.RABBITMQ_QUEUE_CONTROLLER;
        protected override string _path => $"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_PAYMENT_ORDER}";
        protected override TypeMethod _typeMethod => TypeMethod.Put;
    }
}
