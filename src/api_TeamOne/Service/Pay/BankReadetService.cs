﻿using api_TeamOne.Service.Rabbit;
using model_TeamOne;

namespace api_TeamOne.Service.Pay
{
    public class BankReadetService : RabbitReaderService
    {
        public BankReadetService(IHttpClientFactory httpClientFactory) : base(httpClientFactory)
        {
        }
        protected override string _queue => Setting.RABBITMQ_QUEUE_BANK;
        protected override string _path => $"{Setting.BASIC_ADRESS_API}/{Setting.CONTROLLER_BANK}";
        protected override TypeMethod _typeMethod => TypeMethod.Post;
    }
}
