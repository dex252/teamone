﻿using api_TeamOne.Model;
using api_TeamOne.Service.Rabbit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankController : ControllerBase
    {
        private readonly DataBaseContext _context;
        private readonly IRabbitService _rabbitService;
        public BankController(DataBaseContext context, IRabbitService rabbitService)
        {
            _context = context;
            _rabbitService = rabbitService;
        }
        [HttpPost]
        public async Task<ActionResult> Post(RabbitObjectServer obj)
        {
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(Setting.DEBUG_DELAY_FROM_SECOND));
                var order = await _context.PaymentOrders.FindAsync(obj.Id);
                if (order == null) return NotFound();
                if (order.Condition == Condition.Done) return BadRequest("Платёжное поручение уже выполнено.");

                switch (order.TypeOperation)
                {
                    case PaymentOperation.Buy: await OperationBuy(order); break;
                    case PaymentOperation.Sale: await OperationSale(order); break;
                    case PaymentOperation.Transfer: await OperationTransfer(order); break;
                    case PaymentOperation.Convertation: await OperationConvartation(order); break;
                    case PaymentOperation.Withdraw: await OperationWithdraw(order); break;
                    default: return BadRequest();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Покупка акций
        /// </summary>
        private async Task OperationBuy(PaymentOrder order)
        {
            var company = await _context.Companys.FindAsync(order.CompanyId);
            var valute = await _context.Valutes.FindAsync(company.ValuteId);
            var companyValute = await _context.Companys.SingleOrDefaultAsync(s => s.IsValute && s.ValuteId == valute.Id);
            var balanceGet = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == order.UserId && s.CompanyId == companyValute.Id);
            var balanceTake = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == order.UserId && s.CompanyId == company.Id);
            if (order.Sum > balanceGet.Value)
            {
                await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = false, Message = "Недостаточно средств" }, Setting.RABBITMQ_QUEUE_CONTROLLER);
                return;
            }
            else
            {
                balanceGet.Value -= order.Sum;
                _context.Balances.Update(balanceGet);
            }
            if (balanceTake is null)
            {
                await _context.Balances.AddAsync(new Balance()
                {
                    CompanyId = company.Id,
                    UserId = order.UserId,
                    Value = order.Count
                });
            }
            else
            {
                balanceTake.Value += order.Count;
                _context.Balances.Update(balanceTake);
            }
            Transaction transaction = GetTransactionFromOrder(order, TransactionOperation.Withdraw, order.Sum);
            await _context.Transactions.AddAsync(transaction);
            await _context.SaveChangesAsync();
            await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = true }, Setting.RABBITMQ_QUEUE_CONTROLLER);
        }
        /// <summary>
        /// Продажа акций
        /// </summary>
        private async Task OperationSale(PaymentOrder order)
        {
            var company = await _context.Companys.FindAsync(order.CompanyId);
            if (company.IsValute)
            {
                await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = false, Message = "Недоступная операция" }, Setting.RABBITMQ_QUEUE_CONTROLLER);
                return;
            }
            var valute = await _context.Valutes.FindAsync(company.ValuteId);
            var companyValute = await _context.Companys.SingleOrDefaultAsync(s => s.IsValute && s.ValuteId == valute.Id);
            var balanceGet = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == order.UserId && s.CompanyId == company.Id);
            var balanceTake = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == order.UserId && s.CompanyId == companyValute.Id);
            if (balanceGet.Value < order.Count)
            {
                await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = false, Message = "Недостаточно количество акций" }, Setting.RABBITMQ_QUEUE_CONTROLLER);
                return;
            }
            Transaction transaction = GetTransactionFromOrder(order, TransactionOperation.Transfer, order.Sum);
            balanceGet.Value -= order.Count;
            balanceTake.Value += order.Sum;
            await _context.Transactions.AddAsync(transaction);
            _context.Balances.Update(balanceGet);
            _context.Balances.Update(balanceTake);
            await _context.SaveChangesAsync();
            await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = true }, Setting.RABBITMQ_QUEUE_CONTROLLER);

        }
        /// <summary>
        /// Пополнее средств
        /// </summary>
        private async Task OperationTransfer(PaymentOrder order)
        {
            var company = await _context.Companys.FindAsync(order.CompanyId);
            var valute = await _context.Valutes.FindAsync(company.ValuteId);
            var companyValute = await _context.Companys.SingleOrDefaultAsync(s => s.IsValute && s.ValuteId == valute.Id);
            var balanceTake = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == order.UserId && s.CompanyId == companyValute.Id);
            if (balanceTake is null)
            {
                balanceTake = new Balance() { UserId = order.UserId, CompanyId = companyValute.Id, Value = order.Sum };
                await _context.Balances.AddAsync(balanceTake);
            }
            else
            {
                balanceTake.Value += order.Sum;
                _context.Balances.Update(balanceTake);
            }
            Transaction transaction = GetTransactionFromOrder(order, TransactionOperation.Transfer, order.Sum);
            await _context.Transactions.AddAsync(transaction);
            await _context.SaveChangesAsync();
            await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = true }, Setting.RABBITMQ_QUEUE_CONTROLLER);
        }
        /// <summary>
        /// Снятие средств
        /// </summary>
        private async Task OperationWithdraw(PaymentOrder order)
        {
            var company = await _context.Companys.FindAsync(order.CompanyId);
            var valute = await _context.Valutes.FindAsync(company.ValuteId);
            var companyValute = await _context.Companys.SingleOrDefaultAsync(s => s.IsValute && s.ValuteId == valute.Id);
            var balanceGet = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == order.UserId && s.CompanyId == companyValute.Id);
            if (balanceGet is null)
            {
                await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = false, Message = "Не найден счёт" }, Setting.RABBITMQ_QUEUE_CONTROLLER);
                return;
            }
            else if (balanceGet.Value < order.Sum)
            {
                await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = false, Message = "Недостаточно средств" }, Setting.RABBITMQ_QUEUE_CONTROLLER);
                return;
            }
            balanceGet.Value -= order.Sum;
            _context.Balances.Update(balanceGet);
            Transaction transaction = GetTransactionFromOrder(order, TransactionOperation.Withdraw, order.Sum);
            await _context.Transactions.AddAsync(transaction);
            await _context.SaveChangesAsync();
            await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = true }, Setting.RABBITMQ_QUEUE_CONTROLLER);
        }
        /// <summary>
        /// Обмен валюты
        /// </summary>
        private async Task OperationConvartation(PaymentOrder order)
        {
            var company1 = await _context.Companys.FindAsync(order.CompanyId);
            var valute1 = await _context.Valutes.FindAsync(company1.ValuteId);
            var companyValute1 = await _context.Companys.SingleOrDefaultAsync(s => s.IsValute && s.ValuteId == valute1.Id);
            var company2 = await _context.Companys.FindAsync(order.CompanyId2);
            var valute2 = await _context.Valutes.FindAsync(company2.ValuteId);
            var companyValute2 = await _context.Companys.SingleOrDefaultAsync(s => s.IsValute && s.ValuteId == valute2.Id);
            var balanceGet = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == order.UserId && s.CompanyId == companyValute1.Id);
            var balanceTake = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == order.UserId && s.CompanyId == companyValute2.Id);
            if (balanceGet is null)
            {
                await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = false, Message = "Не найден счёт" }, Setting.RABBITMQ_QUEUE_CONTROLLER);
                return;
            }
            else if (balanceGet.Value < order.Sum)
            {
                await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = false, Message = "Недостаточно средств" }, Setting.RABBITMQ_QUEUE_CONTROLLER);
                return;
            }
            balanceGet.Value -= order.Sum;
            _context.Balances.Update(balanceGet);
            if (balanceTake is null)
            {
                balanceTake = new Balance() { UserId = order.UserId, CompanyId = companyValute2.Id, Value = order.Sum2 };
                await _context.Balances.AddAsync(balanceTake);
            }
            else
            {
                balanceTake.Value += order.Sum2;
                _context.Balances.Update(balanceTake);
            }
            Transaction transaction1 = GetTransactionFromOrder(order, TransactionOperation.Withdraw, order.Sum);
            await _context.Transactions.AddAsync(transaction1);
            Transaction transaction2 = GetTransactionFromOrder(order, TransactionOperation.Transfer, order.Sum2);
            await _context.Transactions.AddAsync(transaction2);
            await _context.SaveChangesAsync();
            await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = true }, Setting.RABBITMQ_QUEUE_CONTROLLER);
        }
        private Transaction GetTransactionFromOrder(PaymentOrder order, TransactionOperation transactionOperation, double Sum) => new Transaction()
        {
            UserId = order.UserId,
            CompanyId = order.CompanyId,
            PaymentOrderId = order.Id,
            DateHistory = DateTime.Now,
            TypeOperation = transactionOperation,
            Sum = Sum
        };
    }
}
