﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne.Model.DataBase.Exchange;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyStockController : ControllerBase
    {
        private readonly DataBaseContext _context;
        public CompanyStockController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<CompanyStockPrice>> Get(Guid id, int skip, int take)
        {
            try
            {
                return Ok(await _context.CompanyStockPrices.OrderByDescending(s => s.DateHistory).Where(s => s.CompanyId == id).Skip(skip).Take(take).ToListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("Count")]
        public async Task<ActionResult<long>> Count(Guid id)
        {
            try
            {
                return Ok(await _context.CompanyStockPrices.Where(s => s.CompanyId == id).CountAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
