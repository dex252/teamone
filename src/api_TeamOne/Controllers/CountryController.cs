﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne.Model.DataBase.Exchange;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        DataBaseContext _context;
        public CountryController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Country>>> Get()
        {
            try
            {
                return Ok(await _context.Countrys.Where(s => s.IsDelete == false).ToListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<Country>> Get(Guid id)
        {
            try
            {
                return Ok(await _context.Countrys.FindAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
