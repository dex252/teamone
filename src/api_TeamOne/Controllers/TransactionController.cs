﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne.Model.DataBase.Exchange;
using System.Security.Claims;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TransactionController : ControllerBase
    {

        private readonly DataBaseContext _context;
        public TransactionController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Transaction>>> Get(int skip, int take)
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                return Ok(await _context.Transactions.Where(s => s.UserId == _userId).ToListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
