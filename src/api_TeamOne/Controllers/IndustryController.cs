﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne.Model.DataBase.Exchange;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IndustryController : ControllerBase
    {
        DataBaseContext _context;
        public IndustryController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Industry>>> Get()
        {
            try
            {
                return Ok(await _context.Industrys.Where(s => s.IsDelete == false).ToListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
