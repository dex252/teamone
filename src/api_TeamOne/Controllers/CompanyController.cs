﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne.Model.DataBase.Exchange;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        DataBaseContext _context;
        public CompanyController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Company>>> Get()
        {
            try
            {
                var result = await _context.Companys.Where(s => s.IsDelete == false).ToListAsync();
                foreach (var company in result)
                {
                    if (company.IsValute == false)
                    {
                        company.StockPrice = await _context.CompanyStockPrices.Where(s => s.CompanyId == company.Id && s.DateHistory >= DateTime.Now.AddYears(-1).AddDays(-1)).OrderBy(s => s.DateHistory).ToListAsync();
                        company.Industries = await _context.CmpanyIndustries.Where(s => s.CompanyId == company.Id).Select(s => s.IndustryId).ToListAsync();
                    }
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<Company>> Get(Guid id)
        {
            try
            {
                var result = await _context.Companys.FindAsync(id);
                if (result is not null)
                {
                    if (result.IsValute == false)
                    {
                        result.StockPrice = await _context.CompanyStockPrices.Where(s => s.CompanyId == result.Id && s.DateHistory >= DateTime.Now.AddYears(-1).AddDays(-1)).OrderBy(s => s.DateHistory).ToListAsync();
                        result.Industries = await _context.CmpanyIndustries.Where(s => s.CompanyId == result.Id).Select(s => s.IndustryId).ToListAsync();
                    }
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
