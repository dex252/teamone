﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Security.Claims;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BalanceController : ControllerBase
    {
        private readonly DataBaseContext _context;
        public BalanceController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Balance>>> Get()
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                var result = await _context.Balances.Where(s => s.UserId == _userId).ToListAsync();
                var company = await GetCompanyStock();
                var valute = await GetValue();

                foreach (var balance in result)
                {
                    if (!company[balance.CompanyId].IsValute)
                    {
                        balance.SumBuy = await SumCompany(_userId, balance.CompanyId);
                        balance.SumSale = balance.Value * company[balance.CompanyId].StockPrice.LastOrDefault().Price;
                    }
                    else
                    {
                        balance.SumBuy = 1;
                        balance.SumSale = 1;
                    }
                    balance.Company = company[balance.CompanyId];
                    balance.IsValuteAll = false;
                }

                foreach (var valuteCompany in await _context.Companys.Where(s => s.IsValute == true).ToListAsync())
                {
                    var balance = new Balance();
                    balance.CompanyId = valuteCompany.Id;
                    balance.Company = company[valuteCompany.Id];
                    balance.IsValuteAll = true;
                    balance.SumBuy = 0;
                    balance.SumBuy = result.Where(s => s.IsValuteAll == false && s.Company.ValuteId == valuteCompany.ValuteId).Sum(s => s.SumBuy);
                    balance.SumSale = result.Where(s => s.IsValuteAll == false && s.Company.ValuteId == valuteCompany.ValuteId).Sum(s => s.SumSale);
                    result.Add(balance);
                }

                return Ok(result.OrderBy(s => s.Company.IsValute));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<Balance>> Get(Guid id)
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                var result = await _context.Balances.FirstOrDefaultAsync(s => s.UserId == _userId && s.CompanyId == id);
                if (result is null)
                {
                    result = new Balance();
                    result.UserId = _userId;
                    result.CompanyId = id;
                    result.Value = 0;
                }
                var company = await _context.Companys.FindAsync(id);
                company.StockPrice = await _context.CompanyStockPrices.Where(s => s.CompanyId == company.Id).OrderBy(s => s.DateHistory).ToListAsync();
                if (!company.IsValute)
                {
                    result.SumBuy = await SumCompany(_userId, company.Id);
                    result.SumSale = result.Value * company.StockPrice.LastOrDefault().Price;
                }
                else
                {
                    result.SumBuy = 1;
                    result.SumSale = 1;
                }
                result.Company = company;
                result.IsValuteAll = false;
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private async Task<double> SumCompany(Guid userId, Guid companyId) => 
            await _context.PaymentOrders.Where(s => s.UserId == userId && s.CompanyId == companyId && s.Condition == Condition.Done).SumAsync(s => s.Sum * (s.TypeOperation == PaymentOperation.Buy ? 1 : -1));
        private async Task<Dictionary<Guid, Valute>> GetValue()
        {
            try
            {
                var result = await _context.Valutes.ToListAsync();
                foreach (var resultItem in result)
                {
                    var koef = await _context.ValutesPrices.OrderBy(s => s.DateHistory).FirstOrDefaultAsync(x => x.ValuteId == resultItem.Id);
                    resultItem.KoefBuy = koef.KoefBuy;
                    resultItem.KoefSell = koef.KoefSell;
                    var koefLastDay = await _context.ValutesPrices.Where(s => s.DateHistory <= DateTime.Now.AddDays(-1)).OrderBy(s => s.DateHistory).FirstOrDefaultAsync(x => x.ValuteId == resultItem.Id);
                    resultItem.KoefBuyLastDay = koefLastDay.KoefBuy;
                    resultItem.KoefSellLastDay = koefLastDay.KoefSell;
                }
                return result.ToDictionary(s => s.Id, s => s);
            }
            catch (Exception ex)
            {
                return new Dictionary<Guid, Valute>();
            }
        }
        private async Task<Dictionary<Guid, Company>> GetCompanyStock()
        {
            try
            {
                var result = await _context.Companys.Where(s => s.IsDelete == false).ToListAsync();
                foreach (var company in result)
                {
                    company.StockPrice = await _context.CompanyStockPrices.Where(s => s.CompanyId == company.Id).OrderBy(s => s.DateHistory).ToListAsync();
                }
                return result.ToDictionary(s => s.Id, s => s);
            }
            catch (Exception ex)
            {
                return new Dictionary<Guid, Company>();
            }
        }
    }
}
