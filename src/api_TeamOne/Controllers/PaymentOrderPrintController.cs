﻿using Microsoft.AspNetCore.Mvc;
using jsreport.AspNetCore;
using jsreport.Types;
using api_TeamOne.Repository;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentOrderPrintController : Controller
    {
        private IOrderRepository OrderRepository { get; }
        private IJsReportMVCService JsReportMVCService { get; }

        public PaymentOrderPrintController(IOrderRepository orderRepository, IJsReportMVCService jsReportMVCService)
        {
            OrderRepository = orderRepository;
            JsReportMVCService = jsReportMVCService;
        }

        [HttpGet("{id:Guid}")]
        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var order = await OrderRepository.GetOrder(id);

                HttpContext.JsReportFeature().Recipe(Recipe.ChromePdf)
                    .OnAfterRender((r) => HttpContext.Response.Headers["Content-Disposition"] = $"attachment; filename=\"{order.NumberId}.pdf\"");

                return View("~/Templates/OrderTemplate.cshtml", order);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
       
    }
}
