﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne.Model.DataBase.Exchange;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ValuteController : ControllerBase
    {
        private readonly DataBaseContext _context;
        public ValuteController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Valute>>> Get()
        {
            try
            {
                var result = await _context.Valutes.ToListAsync();
                foreach (var resultItem in result)
                {
                    var koef = await _context.ValutesPrices.OrderBy(s => s.DateHistory).FirstOrDefaultAsync(x => x.ValuteId == resultItem.Id);
                    resultItem.KoefBuy = koef.KoefBuy;
                    resultItem.KoefSell = koef.KoefSell;
                    var koefLastDay = await _context.ValutesPrices.Where(s => s.DateHistory <= DateTime.Now.AddDays(-1)).OrderBy(s => s.DateHistory).FirstOrDefaultAsync(x => x.ValuteId == resultItem.Id);
                    resultItem.KoefBuyLastDay = koefLastDay.KoefBuy;
                    resultItem.KoefSellLastDay = koefLastDay.KoefSell;
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<Valute>> Get(Guid id)
        {
            try
            {
                return Ok(await _context.Valutes.FindAsync(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
