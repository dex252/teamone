﻿using Api.Brokers;
using Api.Brokers.Enums;
using Api.Brokers.Generator;
using Api.Brokers.Models.Finhub;
using Api.Brokers.Test.Unit.TestData;
using api_TeamOne.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Reflection;
using System.Security.Claims;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    ///Для теста, заполнить рандомными данными
    public class TestConditionController : ControllerBase
    {

        private readonly DataBaseContext _context;
        private Random _rnd;
        public TestConditionController(DataBaseContext context)
        {
            _context = context;
            _rnd = new Random();
        }
        [HttpPost]
        public async Task<ActionResult> Post()
        {
            try
            {
                FormattableString sqlQuery = $"TRUNCATE TABLE Companys;TRUNCATE TABLE CompanyStockPrices;TRUNCATE TABLE Valutes;TRUNCATE TABLE ValutesPrices;TRUNCATE TABLE Transactions;TRUNCATE TABLE Industrys;TRUNCATE TABLE CmpanyIndustries;TRUNCATE TABLE PaymentOrders;TRUNCATE TABLE Countrys;TRUNCATE TABLE Balances;";
                await _context.Database.ExecuteSqlAsync(sqlQuery);
                await CreateCountry();
                await CreateIndustry();
                await CreateValute();
                await CreateValuteHistory();
                await CreateCompany();
                await CreateCompanyIndustry();
                await CreateCompanyHistory(1500);
                await CreateBalance();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost("UserBalance")]
        public async Task<ActionResult> PostUserBalance()
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                var companyList = await _context.Companys.Where(s => s.IsValute == false && s.IsDelete == false).ToListAsync();
                var valuteHistory = await _context.ValutesPrices.OrderBy(s => s.DateHistory).ToListAsync();
                foreach (var company in companyList)
                {
                    company.StockPrice = await _context.CompanyStockPrices.Where(s => s.CompanyId == company.Id).OrderByDescending(s => s.DateHistory).ToListAsync();
                    var valuteCompnay = await _context.Companys.FirstOrDefaultAsync(s => s.IsValute == true && s.IsDelete == false && s.ValuteId == company.ValuteId);
                    for (DateTime period = company.StockPrice.Min(s => s.DateHistory); period < DateTime.Now.AddDays(1); period = period.AddDays(_rnd.Next(10, 30)))
                    {
                        var count = _rnd.Next(1, 5) * company.Package;
                        var price = company.StockPrice.FirstOrDefault(s => s.DateHistory <= period).Price * count;
                        //Добавим на баланс деньги
                        //Если первый раз добавляем, то создадим пустой баланс
                        var balance1 = await _context.Balances.SingleOrDefaultAsync(s => s.UserId == _userId && s.CompanyId == valuteCompnay.Id);
                        if (balance1 == null)
                        {
                            await _context.Balances.AddAsync(new Balance()
                            {
                                UserId = _userId,
                                CompanyId = valuteCompnay.Id,
                                Value = 0
                            });
                            await _context.SaveChangesAsync();
                        }
                        await AddBalanceValute(_userId, valuteHistory, valuteCompnay, period, price);

                        //Если первый раз полкупаем, то создадим пустой баланс
                        var balance2 = await _context.Balances.SingleOrDefaultAsync(s => s.UserId == _userId && s.CompanyId == company.Id);
                        if (balance2 == null)
                        {
                            await _context.Balances.AddAsync(new Balance()
                            {
                                UserId = _userId,
                                CompanyId = company.Id,
                                Value = 0
                            });
                            await _context.SaveChangesAsync();
                        }
                        //Купи акцию
                        await AddStock(_userId, company, valuteCompnay, count, price, period);
                    }
                }
                return Ok();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private async Task AddStock(Guid _userId, Company? company, Company? valuteCompnay, int count, double price, DateTime period)
        {
            var paymentOrder1 = new PaymentOrder()
            {
                UserId = _userId,
                CompanyId = company.Id,
                DateHistory = period,
                TypeOperation = PaymentOperation.Buy,
                Condition = Condition.Done,
                Sum = price,
                Count = count
            };
            var transaction1 = new model_TeamOne.Model.DataBase.Exchange.Transaction()
            {
                UserId = _userId,
                CompanyId = valuteCompnay.Id,
                PaymentOrderId = paymentOrder1.Id,
                DateHistory = period,
                TypeOperation = TransactionOperation.Withdraw,
                Sum = price
            };
            var balance1 = await _context.Balances.SingleOrDefaultAsync(s => s.UserId == _userId && s.CompanyId == valuteCompnay.Id);
            balance1.Value = balance1.Value - price;
            var balance2 = await _context.Balances.SingleOrDefaultAsync(s => s.UserId == _userId && s.CompanyId == company.Id);
            balance2.Value = balance2.Value + count;
            await _context.PaymentOrders.AddAsync(paymentOrder1);
            await _context.Transactions.AddAsync(transaction1);
            await _context.SaveChangesAsync();
        }
        private async Task AddBalanceValute(Guid _userId, List<ValutePrice> valuteHistory, Company? valuteCompnay, DateTime period, double price)
        {
            var priceBuff = price * 1.01;
            var paymentOrder1 = new PaymentOrder()
            {
                UserId = _userId,
                CompanyId = valuteCompnay.Id,
                DateHistory = period,
                TypeOperation = PaymentOperation.Transfer,
                Condition = Condition.Done,
                Sum = priceBuff
            };
            var transaction1 = new model_TeamOne.Model.DataBase.Exchange.Transaction()
            {
                UserId = _userId,
                CompanyId = valuteCompnay.Id,
                TypeOperation = TransactionOperation.Transfer,
                PaymentOrderId = paymentOrder1.Id,
                DateHistory = period,
                Sum = priceBuff
            };
            var balance1 = await _context.Balances.SingleOrDefaultAsync(s => s.UserId == _userId && s.CompanyId == valuteCompnay.Id);
            balance1.Value = balance1.Value + priceBuff;
            await _context.PaymentOrders.AddAsync(paymentOrder1);
            await _context.Transactions.AddAsync(transaction1);
            await _context.SaveChangesAsync();
        }

        private async Task CreateCountry()
        {
            await _context.Countrys.AddAsync(new Country { Name = "Россия" });
            await _context.Countrys.AddAsync(new Country { Name = "Соединенные Штаты" });
            await _context.SaveChangesAsync();
        }
        private async Task CreateIndustry()
        {
            await _context.Industrys.AddAsync(new Industry() { Name = "Информационные технологии" });
            await _context.Industrys.AddAsync(new Industry() { Name = "Финансовый сектор" });
            await _context.Industrys.AddAsync(new Industry() { Name = "Потребительские товары и услуги" });
            await _context.Industrys.AddAsync(new Industry() { Name = "Телекоммуникации" });
            await _context.SaveChangesAsync();
        }
        private async Task CreateValute()
        {
            await _context.Valutes.AddAsync(new Valute() { Name = "Рубль", Symbol = "₽" });
            await _context.Valutes.AddAsync(new Valute() { Name = "Доллар", Symbol = "$" });
            await _context.Valutes.AddAsync(new Valute() { Name = "Евро", Symbol = "€" });
            await _context.SaveChangesAsync();
        }
        private async Task CreateValuteHistory()
        {
            var valuteList = await _context.Valutes.ToListAsync();
            double koefSell = 1;
            double koekBuy = 1;
            for (DateTime period = new DateTime(2022, 1, 1); period < DateTime.Now.AddDays(1); period = period.AddDays(1))
            {
                foreach (var valute in valuteList)
                {
                    if (valute.Name == "Рубль")
                    {
                        koefSell = 1;
                        koekBuy = 1;
                    }
                    if (valute.Name == "Доллар")
                    {
                        koefSell = _rnd.Next(70, 100) + _rnd.NextDouble();
                        koekBuy = koefSell - _rnd.Next(15);
                    }
                    if (valute.Name == "Евро")
                    {
                        koefSell = _rnd.Next(80, 130) + _rnd.NextDouble();
                        koekBuy = koefSell - _rnd.Next(15);
                    }
                    await _context.ValutesPrices.AddAsync(new ValutePrice()
                    {
                        ValuteId = valute.Id,
                        DateHistory = period,
                        KoefBuy = koekBuy,
                        KoefSell = koefSell
                    });
                }
            }
            await _context.SaveChangesAsync();
        }
        private async Task CreateCompany()
        {
            var valuteList = await _context.Valutes.ToDictionaryAsync(s => s.Name, s => s.Id);
            var countryList = await _context.Countrys.ToDictionaryAsync(s => s.Name, s => s.Id);
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "RUB", Title = "RUB", Description = "RUB", Adress = "RUB", IsValute = true, Package = 1, CountryId = countryList["Россия"], ValuteId = valuteList["Рубль"] });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "DOL", Title = "DOL", Description = "DOL", Adress = "DOL", IsValute = true, Package = 1, CountryId = countryList["Россия"], ValuteId = valuteList["Доллар"] });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "EUR", Title = "EUR", Description = "EUR", Adress = "EUR", IsValute = true, Package = 1, CountryId = countryList["Россия"], ValuteId = valuteList["Евро"] });

            Guid DOLId = await _context.Valutes.Where(s => s.Name == "Доллар").Select(s => s.Id).FirstOrDefaultAsync();
            Guid EURId = await _context.Valutes.Where(s => s.Name == "Евро").Select(s => s.Id).FirstOrDefaultAsync();
            Guid RUBId = await _context.Valutes.Where(s => s.Name == "Рубль").Select(s => s.Id).FirstOrDefaultAsync();
            Guid USACountryId = await _context.Countrys.Where(s => s.Name == "Соединенные Штаты").Select(s => s.Id).FirstOrDefaultAsync();
            Guid RUSCountryId = await _context.Countrys.Where(s => s.Name == "Россия").Select(s => s.Id).FirstOrDefaultAsync();

            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "Apple", Title = "AAPL", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = USACountryId, ValuteId = DOLId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "Amazon.com", Title = "AMZN", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = USACountryId, ValuteId = DOLId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "Berkshire Hathaway", Title = "BRK-A", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = USACountryId, ValuteId = EURId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "Facebook", Title = "FB", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = USACountryId, ValuteId = DOLId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "Alphabet Class C", Title = "GOOGL", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = USACountryId, ValuteId = DOLId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "IBM", Title = "IBM", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = USACountryId, ValuteId = DOLId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "JPMorgan", Title = "JPM", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = USACountryId, ValuteId = EURId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "Microsoft Corporation", Title = "MSFT", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = USACountryId, ValuteId = DOLId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "Procter & Gamble", Title = "PG", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = RUSCountryId, ValuteId = RUBId });
            await _context.Companys.AddAsync(new Company() { Avatar = "", Name = "Tesla Motors", Title = "TSLA", Description = "Описание", Adress = "", IsValute = false, Package = 1, CountryId = RUSCountryId, ValuteId = RUBId });
            await _context.SaveChangesAsync();
        }
        private async Task CreateCompanyIndustry()
        {
            var industries = await _context.Industrys.ToListAsync();

            foreach (var company in await _context.Companys.Where(s => s.IsValute == false).ToListAsync())
            {
                Guid IndustriesId = Guid.NewGuid();
                if (company.Title == "AAPL") IndustriesId = industries.FirstOrDefault(s => s.Name == "Информационные технологии").Id;
                if (company.Title == "AMZN") IndustriesId = industries.FirstOrDefault(s => s.Name == "Потребительские товары и услуги").Id;
                if (company.Title == "BRK-A") IndustriesId = industries.FirstOrDefault(s => s.Name == "Финансовый сектор").Id;
                if (company.Title == "FB") IndustriesId = industries.FirstOrDefault(s => s.Name == "Информационные технологии").Id;
                if (company.Title == "GOOGL") IndustriesId = industries.FirstOrDefault(s => s.Name == "Телекоммуникации").Id;
                if (company.Title == "IBM") IndustriesId = industries.FirstOrDefault(s => s.Name == "Финансовый сектор").Id;
                if (company.Title == "MSFT") IndustriesId = industries.FirstOrDefault(s => s.Name == "Информационные технологии").Id;
                if (company.Title == "PG") IndustriesId = industries.FirstOrDefault(s => s.Name == "Потребительские товары и услуги").Id;
                if (company.Title == "TSLA") IndustriesId = industries.FirstOrDefault(s => s.Name == "Потребительские товары и услуги").Id;
                if (company.Title == "JPM") IndustriesId = industries.FirstOrDefault(s => s.Name == "Финансовый сектор").Id;

                await _context.CmpanyIndustries.AddAsync(new CompanyIndustry()
                {
                    CompanyId = company.Id,
                    IndustryId = IndustriesId
                });
            }
            await _context.SaveChangesAsync();
        }
        private async Task CreateBalance()
        {
            Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
            var companyList = await _context.Companys.Where(s => s.IsDelete == false && s.IsValute == true).ToListAsync();
            foreach (var company in companyList)
            {
                await _context.Balances.AddAsync(new Balance()
                {
                    UserId = _userId,
                    CompanyId = company.Id,
                    Value = 0
                });
            }
            await _context.SaveChangesAsync();
        }
        private async Task CreateCompanyHistory(int generateByLastRowsCount = 150)
        {
            //var allData = new List<List<Row>>();
            var historyData = new GetHistoryData();
            var companys = await _context.Companys.Where(s => s.IsValute == false).ToListAsync();

            foreach (object[] data in historyData)
            {
                if (!companys.Any(s => s.Title == data[0].ToString())) continue;
                // Ваш код для обработки каждого элемента данных
                string faileName = (string)data[1];

                var brokerClient = new BrokerClient();
                var currentBroker = Broker.Finnhub;
                var content = GetFileContent(faileName);
                var set = new KeyValuePair<Broker, string>(currentBroker, content);
                var source = brokerClient.ParseSourceResult<FinhubSourceSet>(set);
                var model = new FinhubSet(source);

                var generator = new GeneratorManager(generateByLastRowsCount);
                var generatedRows = generator.GenerateData<FinhubSet, Row>(model, currentBroker).ToList();

                Guid companyId = companys.FirstOrDefault(s => s.Title == data[0].ToString()).Id;
                foreach (var row in generatedRows)
                {
                    await _context.CompanyStockPrices.AddAsync(new CompanyStockPrice()
                    {
                        CompanyId = companyId,
                        DateHistory = row.Timestamp,
                        Price = _rnd.Next((int)row.MinCost, (int)row.MaxCost) + _rnd.NextDouble(),
                        PriceOpen = row.OpenCost,
                        PriceClose = row.CloseCost,
                        PriceMin = row.MinCost,
                        PriceMax = row.MaxCost,
                    });
                }
                //allData.Add(generatedRows);
            }
            await _context.SaveChangesAsync();
            //return Ok(allData);
        }
        protected string GetFileContent(string fileName)
        {
            var assembly = Assembly.GetAssembly(typeof(GetHistoryData));
            var assambleyName = assembly?.GetManifestResourceNames()
                .FirstOrDefault(e => e.EndsWith(fileName));

            if (assambleyName == null)
                throw new Exception($"Файл {fileName} не найден");

            using (var stream = assembly?.GetManifestResourceStream(assambleyName))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
