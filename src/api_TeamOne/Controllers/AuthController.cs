﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using model_TeamOne;
using model_TeamOne.Model.Auth;
using model_TeamOne.Model.DataBase.Account;
using model_TeamOne.Model.DataBase.Exchange;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private readonly DataBaseContext _context;
        public AuthController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpPost("/registration")]
        [AllowAnonymous]
        public async Task<ActionResult<LoginResponse>> Post(RegistrationModel user)
        {
            if (user == null) return BadRequest();
            if (!ModelState.IsValid) return BadRequest();
            if (await _context.Users.AnyAsync(s => s.Login == user.Login)) return BadRequest();

            var _user = new User()
            {
                Login = user.Login,
                Name = user.Login,
                Password = user.Password,
                Theme = "standard"
            };
            await _context.Users.AddAsync(_user);

            var companyList = await _context.Companys.Where(s => s.IsDelete == false && s.IsValute == true).ToListAsync();
            foreach (var company in companyList)
            {
                await _context.Balances.AddAsync(new Balance()
                {
                    UserId = _user.Id,
                    CompanyId = company.Id,
                    Value = 0
                });
            }

            await _context.SaveChangesAsync();

            return Ok(new LoginResponse()
            {
                Token = CreateJWT(GetClaims(_user)),
                RefreshToken = _user.Id
            });
        }
        [HttpPost("/login")]
        [AllowAnonymous]
        public async Task<ActionResult<LoginResponse>> Post(AuthModel user)
        {
            if (user == null) return BadRequest();
            if (!ModelState.IsValid) return BadRequest();

            var _user = await _context.Users.SingleOrDefaultAsync(s => s.Login == user.Login);
            if (_user == null) return NotFound();
            if (_user.Password != user.Password) return BadRequest();

            return Ok(new LoginResponse()
            {
                Token = CreateJWT(GetClaims(_user)),
                RefreshToken = _user.Id
            }); ;
        }
        [HttpGet("/updatetoken")]
        public async Task<ActionResult<LoginResponse>> Get()
        {
            Guid userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
            var _user = await _context.Users.SingleOrDefaultAsync(s => s.Id == userId);
            return Ok(new LoginResponse()
            {
                Token = CreateJWT(GetClaims(_user)),
                RefreshToken = _user.Id
            });
        }
        [HttpGet("/updatetheme/{theme}")]
        public async Task<ActionResult<bool>> UpdateScheme(string theme)
        {
            try
            {
                Guid userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                var _user = await _context.Users.SingleOrDefaultAsync(s => s.Id == userId);
                _user.Theme = theme;
                await _context.SaveChangesAsync();
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private string CreateJWT(IEnumerable<Claim> claims)
        {
            var token = new JwtSecurityToken(
            issuer: AuthOptions.ISSUER,
            audience: AuthOptions.AUDIENCE,
                    claims: claims,
                    expires: DateTime.UtcNow.Add(Setting.BASE_TOKEN_TIME),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        private Claim[] GetClaims(User _user)
        {
            return new[]
            {
                new Claim(ClaimTypes.Name, _user.Login),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(ClaimTypes.NameIdentifier, _user.Id.ToString()),
                new Claim("Theme", _user.Theme)
            };
        }
    }
}
