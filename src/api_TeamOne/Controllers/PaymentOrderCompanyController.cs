﻿using api_TeamOne.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using model_TeamOne.Model.DataBase.Exchange;
using System.Security.Claims;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PaymentOrderCompanyController : ControllerBase
    {
        private readonly DataBaseContext _context;
        public PaymentOrderCompanyController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PaymentOrder>>> Get(Guid id, int skip, int take)
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                return Ok(await _context.PaymentOrders.Where(s => s.IsDelete == false && s.UserId == _userId && s.CompanyId == id).OrderByDescending(s => s.DateHistory).Skip(skip).Take(take).ToListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("Count")]
        public async Task<ActionResult<int>> Count(Guid id)
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                return Ok(await _context.PaymentOrders.Where(s => s.IsDelete == false && s.UserId == _userId && s.CompanyId == id).CountAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
