﻿using api_TeamOne.Model;
using api_TeamOne.Service.Hubs;
using api_TeamOne.Service.Rabbit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.EntityFrameworkCore;
using model_TeamOne;
using model_TeamOne.Model.DataBase.Exchange;
using System.Security.Claims;

namespace api_TeamOne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PaymentOrderController : ControllerBase
    {
        private readonly DataBaseContext _context;
        private readonly IRabbitService _rabbitService;
        private readonly IHubContext<NotificationHub> _hub;
        public PaymentOrderController(DataBaseContext context, IRabbitService rabbitService, IHubContext<NotificationHub> hub)
        {
            _context = context;
            _rabbitService = rabbitService;
            _hub = hub;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PaymentOrder>>> Get(int skip, int take)
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                return Ok(await _context.PaymentOrders.Where(s => s.IsDelete == false && s.UserId == _userId).OrderByDescending(s => s.DateHistory).Skip(skip).Take(take).ToListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<PaymentOrder>> Get(Guid id)
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                return Ok(await _context.PaymentOrders.FirstOrDefaultAsync(s => s.IsDelete == false && s.UserId == _userId && s.Id == id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ActionResult<PaymentOrder>> Post(PaymentOrder order)
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                order.UserId = _userId;
                order.DateHistory = DateTime.Now;
                order.Condition = Condition.IsBusy;
                await _context.PaymentOrders.AddAsync(order);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            await _rabbitService.SendMessage(new RabbitObjectServer() { Id = order.Id, IsComplite = false }, Setting.RABBITMQ_QUEUE_BANK);
            return Ok(order);
        }
        [AllowAnonymous]
        [HttpPut]
        public async Task<ActionResult> Put(RabbitObjectServer obj)
        {
            await Task.Delay(TimeSpan.FromSeconds(Setting.DEBUG_DELAY_FROM_SECOND));
            try
            {
                //Обновить ПП
                var order = await _context.PaymentOrders.FindAsync(obj.Id);
                if (order == null) return NotFound();
                var transaction = await _context.Transactions.Where(s => s.PaymentOrderId == order.Id).ToListAsync();
                order.MessageError = obj.Message;
                if (transaction.Count() == 0 || !obj.IsComplite)
                {
                    order.Condition = Condition.Fail;
                }
                else
                {
                    order.Condition = Condition.Done;
                }
                _context.PaymentOrders.Update(order);
                await _context.SaveChangesAsync();
                await WriteUser(order.UserId, order.Id, obj.IsComplite ? "Complite" : obj.Message);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private async Task WriteUser(Guid userId, Guid orderId, string message)
        {
            await _hub.Clients.All.SendAsync(userId.ToString(), orderId, message);
        }
        [Route("Count")]
        [HttpGet]
        public async Task<ActionResult<int>> Count()
        {
            try
            {
                Guid _userId = Guid.Parse(User.Claims.FirstOrDefault(s => s.Type == ClaimTypes.NameIdentifier).Value);
                return Ok(await _context.PaymentOrders.Where(s => s.IsDelete == false && s.UserId == _userId).CountAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
