﻿using Microsoft.EntityFrameworkCore;
using model_TeamOne.Model.DataBase.Account;
using model_TeamOne.Model.DataBase.Exchange;

namespace api_TeamOne.Model
{
    public class DataBaseContext : DbContext
    {
        #region Account
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        #endregion
        #region Exchange
        public DbSet<Country> Countrys { get; set; }
        public DbSet<Industry> Industrys { get; set; }
        public DbSet<Company> Companys { get; set; }
        public DbSet<CompanyIndustry> CmpanyIndustries { get; set; }
        public DbSet<Valute> Valutes { get; set; }
        public DbSet<ValutePrice> ValutesPrices { get; set; }
        public DbSet<CompanyStockPrice> CompanyStockPrices { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Balance> Balances { get; set; }
        public DbSet<PaymentOrder> PaymentOrders { get; set; }
        #endregion
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
