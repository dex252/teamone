using api_TeamOne.Model;
using api_TeamOne.Service.Hubs;
using api_TeamOne.Service.Pay;
using api_TeamOne.Service.Rabbit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using model_TeamOne;
using model_TeamOne.Model.Auth;
using jsreport.AspNetCore;
using jsreport.Binary;
using jsreport.Local;
using api_TeamOne.Repository;

var builder = WebApplication.CreateBuilder(args);

////Подключение к Postgres
//string connectionString = $"Host={Setting.POSTGRS_HOST};Port={Setting.POSTGRS_PORT};Database={Setting.POSTGRS_DATABASE};Username={Setting.POSTGRS_LOGIN};Password={Setting.POSTGRS_PASSWORD}";
//!ВНИМАНИЕ Для пост гри надо кинуть настройку AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
//builder.Services.AddDbContext<DataBaseContext>(options => options.UseNpgsql(connectionString).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
//Подключение к локальной БД, с технологией MS SQL
//Он просто создаст файл в папке пользователя (у меня C:\Users\Arkady)
//string connectionString = $"Server={Setting.LOCALDB_SERVER};Database={Setting.LOCALDB_DATABASE};Trusted_Connection=True;";
//builder.Services.AddDbContext<DataBaseContext>(options => options.UseSqlServer(connectionString).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
string connectionString = $"Server=(local);Database={Setting.MS_SQL_DATABASE};Integrated Security=False;User ID={Setting.MS_SQL_LOGIN};Password={Setting.MS_SQL_PASSWORD};TrustServerCertificate=True;MultipleActiveResultSets=True;";
builder.Services.AddDbContext<DataBaseContext>(options => options
                                                            .UseSqlServer(connectionString)
                                                            //.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                                                            );

builder.Services.AddHostedService<BankReadetService>();
builder.Services.AddHostedService<PaymentReaderService>();

builder.Services.AddScoped<IRabbitService, RabbitService>();
builder.Services.AddScoped<IOrderRepository, OrderRepository>();
//builder.Services.AddScoped<IScopedProcessingService, ScopedProcessingService>();
builder.Services.AddSignalR();

builder.Services.AddHttpClient("_myHttpClient", c => c.BaseAddress = new Uri(Setting.BASIC_ADRESS_API));

// разрешение CORS
// я плохо разбираюсь в протоколе HTTP
// и вроде как это должно помочь.
var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
        builder =>
        {
            builder.WithOrigins("*")
              .AllowAnyOrigin()
              .AllowAnyMethod()
              .AllowAnyHeader()
              //.SetIsOriginAllowedToAllowWildcardSubdomains()
              ;
        });
});

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            // указывает, будет ли валидироватьс¤ издатель при валидации токена
            ValidateIssuer = true,
            // строка, представл¤юща¤ издател¤
            ValidIssuer = AuthOptions.ISSUER,
            // будет ли валидироватьс¤ потребитель токена
            ValidateAudience = true,
            // установка потребител¤ токена
            ValidAudience = AuthOptions.AUDIENCE,
            // будет ли валидироватьс¤ врем¤ существовани¤
            ValidateLifetime = true,
            // установка ключа безопасности
            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
            // валидаци¤ ключа безопасности
            ValidateIssuerSigningKey = true
        };
    });

builder.Services.AddAuthorization(options =>
{
    options.DefaultPolicy = new AuthorizationPolicyBuilder()
    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
    .RequireAuthenticatedUser()
    .Build();
});

builder.Services.AddControllersWithViews();
builder.Services.AddJsReport(new LocalReporting().UseBinary(JsReportBinary.GetBinary()).KillRunningJsReportProcesses().AsUtility().Create());

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(MyAllowSpecificOrigins);

app.UseAuthorization();
app.UseAuthentication();

app.MapControllers();
app.MapHub<NotificationHub>("/notification");

app.Run();
