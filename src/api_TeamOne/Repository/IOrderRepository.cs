﻿using model_TeamOne.Model;

namespace api_TeamOne.Repository
{
    public interface IOrderRepository
    {
        Task<PaymentOrderPrint?> GetOrder(Guid id);
    }
}
