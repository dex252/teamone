﻿using api_TeamOne.Model;
using model_TeamOne;
using model_TeamOne.Model;

namespace api_TeamOne.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly DataBaseContext _context;

        public OrderRepository(DataBaseContext context)
        {
            _context = context;
        }

        public async Task<PaymentOrderPrint?> GetOrder(Guid id)
        {
            var result = new PaymentOrderPrint();
            var order = await _context.PaymentOrders.FindAsync(id);
            var user = await _context.Users.FindAsync(order.UserId);
            var company1 = await _context.Companys.FindAsync(order.CompanyId);
            var valute1 = await _context.Valutes.FindAsync(company1.ValuteId);
            var valute2 = valute1;
            if (order.TypeOperation == PaymentOperation.Convertation)
            {
                var company2 = await _context.Companys.FindAsync(order.CompanyId2);
                valute2 = await _context.Valutes.FindAsync(company2.ValuteId);
            }

            result.Id = order.Id;
            result.NumberId = order.NumberId;
            result.Client = user.Name;
            result.ClientBank = user.NumberId;
            result.TypeOperation = order.TypeOperation;
            result.Condition = order.Condition;
            result.MessageError = order.MessageError;
            if (order.TypeOperation != PaymentOperation.Convertation)
            {
                result.Company = company1.Name;
                result.CompanyBank = company1.NumberId;
                result.Count = order.Count;
            }
            result.Sum = order.Sum;
            result.Valute = valute1.Name;
            if (order.TypeOperation == PaymentOperation.Convertation)
            {
                result.Sum2 = order.Sum2;
                result.Valute2 = valute2.Name;
            }
            result.DateOrder = order.DateHistory;
            result.ImagesBase64 = new Dictionary<string, string>();
            var base64 = ConvertImageToBase64("Content/sign.png");
            result.ImagesBase64.Add("sign", base64);
            return result;
        }

        private string ConvertImageToBase64(string imagePath)
        {
            byte[] imageBytes = System.IO.File.ReadAllBytes(imagePath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }
}
