﻿namespace PdfReport.Models
{
    public class Check
    {
        public List<Document> Documents { get; set; }
    }

    public class Document
    {
        public string DocumentId { get; set; }
        public string DocumentDate { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Company { get; set; }
        public decimal CountActions { get; set; }
        public string Cost { get; set; }
        public string TransactionDate { get; set; }
    }
}
