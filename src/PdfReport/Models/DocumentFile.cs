﻿namespace PdfReport.Models
{
    public class DocumentFile
    {
        public string Name { get; set; }
        public string Extension { get; set; }

        public byte[] Content { get; set; }

        public string FullName => $"{Name}.{Extension}";
    }
}
