using FastReport;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using PdfReport.Models;
using FastReport.Export;
using FastReport.Export.PdfSimple;
using System.Reflection;
using FastReport.Utils;
using FastReport.Data;
using FastReport.Data.JsonConnection;
using Microsoft.AspNetCore.StaticFiles;

namespace PdfReport.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PrintController : ControllerBase
    {
        private ILogger<PrintController> Log { get; }
        private IWebHostEnvironment Environment { get; }

        public PrintController(ILogger<PrintController> log, IWebHostEnvironment environment)
        {
            Log = log;
            Environment = environment;
        }

        /// <summary>
        /// ������ ��������� �� id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get(double documentId)
        {
            RegisteredObjects.AddConnection(typeof(JsonDataConnection));

            var report = new Report();
            var templatePath = Path.Combine(Environment.ContentRootPath, "Templates/CheckJson.frx");
            var builder = new JsonDataSourceConnectionStringBuilder();

#if !MOCK
            //TODO: �� documentId �������� ������ ������ ��� ������
            var model = new Check();
            builder.Json = JsonConvert.SerializeObject(model);
#else
            builder.Json = GetFileContent("report.json");
#endif

            var connection = new JsonDataSourceConnection();
            connection.ConnectionString = builder.ToString();
            report.Dictionary.Connections.Add(connection);
            connection.CreateAllTables();

            report.Load(templatePath);
            report.Prepare();

            var document = new DocumentFile();
            document.Name = $"pay_document-{documentId}";
            document.Extension = "pdf";

            var provider = new FileExtensionContentTypeProvider();
            var isParse = provider.TryGetContentType(document.FullName, out var mimeType);
            if (!isParse)
            {
                throw new Exception($"��� ����� ��� {documentId} �� ���������");
            }

            using (var stream = new MemoryStream())
            {
                var export = new PDFSimpleExport();
                report.Export(export, stream);
                stream.Position = 0;

                document.Content = stream.ToArray();
            }

            Response.Headers.Add("Content-Disposition", $"attachment; filename=\"{document.FullName}\"");
            return File(document.Content, mimeType);
        }

        protected string GetFileContent(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var assambleyName = assembly?.GetManifestResourceNames()
                .FirstOrDefault(e => e.EndsWith(fileName));

            if (assambleyName == null)
                throw new Exception($"���� {fileName} �� ������");

            using (var stream = assembly?.GetManifestResourceStream(assambleyName))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}