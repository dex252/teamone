﻿using System.Reflection;

namespace Api.Brokers.Test.Unit
{
    public class BaseTest
    {
        protected BrokerClient BrokerClient { get; }

        public BaseTest()
        {
            BrokerClient = new BrokerClient();
        }

        protected string GetFileContent(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var assambleyName = assembly.GetManifestResourceNames()
                .FirstOrDefault(e => e.EndsWith(fileName));

            if(assambleyName == null)
                throw new Exception($"Файл {fileName} не найден");

            using (var stream = assembly.GetManifestResourceStream(assambleyName))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
