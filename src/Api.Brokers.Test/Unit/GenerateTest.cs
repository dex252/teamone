﻿using Api.Brokers.Enums;
using Api.Brokers.Generator;
using Api.Brokers.Models.Finhub;
using Api.Brokers.Test.Unit.TestData;
using Xunit;

namespace Api.Brokers.Test.Unit
{
    public class GenerateTest : BaseTest
    {

        [Theory]
        [ClassData(typeof(GetHistoryData))]
        public void GenerateTestData(string companyCode, string faileName, string displayCompanyName, int countRows = 100) 
        {
            var currentBroker = Broker.Finnhub;
            var content = GetFileContent(faileName);
            var set = new KeyValuePair<Broker, string>(currentBroker, content);
            var source = BrokerClient.ParseSourceResult<FinhubSourceSet>(set);
            var model = new FinhubSet(source);

            var generator = new GeneratorManager(countRows);
            var generatedRows = generator.GenerateData<FinhubSet, Row>(model, currentBroker);

            Assert.NotNull(generatedRows);
            Assert.True(generatedRows.Count() > 0);
        }
    }
}
