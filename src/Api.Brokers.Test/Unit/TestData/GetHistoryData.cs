﻿using System.Collections;

namespace Api.Brokers.Test.Unit.TestData
{
    public class GetHistoryData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { "AAPL", "Finhub_AAPL.json", "Apple Inc.", 100};
            yield return new object[] { "IBM", "Finhub_IBM.json",  "IBM Inc.", 10};
            yield return new object[] { "MSFT", "Finhub_MSFT.json", "Microsoft Corporation" , 5};
            yield return new object[] { "AMZN", "Finhub_AMZN.json", "Amazon.com, Inc." };
            yield return new object[] { "GOOGL", "Finhub_GOOGLE.json", "Alphabet Inc. (Google) Class A" };
            yield return new object[] { "FB", "Finhub_Facebook.json", "Facebook, Inc." };
            yield return new object[] { "TSLA", "Finhub_Tesla.json", "Tesla, Inc." };
            yield return new object[] { "JPM", "Finhub_JPM.json", "JPMorgan Chase & Co." };
            yield return new object[] { "PG", "Finhub_PG.json", "Johnson & Johnson- JNJ 10.Procter & Gamble Company (The)" };
            yield return new object[] { "BRK-A", "Finhub_BRK.json", "Berkshire Hathaway Inc" };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
