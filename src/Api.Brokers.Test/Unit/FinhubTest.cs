using Api.Brokers.Enums;
using Api.Brokers.Models.Finhub;
using Xunit;
using Api.Brokers.Extensions;
using Api.Brokers.Test.Unit.TestData;

namespace Api.Brokers.Test.Unit
{
    public class FinhubTest : BaseTest
    {
        /// <summary>
        /// Успешный код ответа от Finhub
        /// </summary>
        const string OK_RESULT = "ok";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyCode">Код компании на Finhub</param>
        /// <param name="faileName">Слепок исторических данных с 01.01.13 по 01.01.2023</param>
        /// <param name="displayCompanyName">Название компании</param>
        /// <returns></returns>
        [Theory]
        [ClassData(typeof(GetHistoryData))]
        public async Task GetHistoryAsync(string companyCode, string faileName, string displayCompanyName)
        {
            await Console.Out.WriteLineAsync(displayCompanyName);
            var parameters = new Dictionary<string, string>();
            var currentBroker = Broker.Finnhub;

            var resolution = "D";//1, 5, 15, 30, 60, D, W, M - резолюция длительности от 1 минуты до месяца
            var startDate = new DateTime(2013, 01, 01).ToUnixTime();
            var endDate = new DateTime(2023, 01, 01).ToUnixTime();

            parameters.Add($"{currentBroker}_SYMBOL", companyCode);
            parameters.Add($"{currentBroker}_RESOLUTION", resolution);
            parameters.Add($"{currentBroker}_FROM_DATE", startDate.ToString());
            parameters.Add($"{currentBroker}_TO_DATE", endDate.ToString());

#if MOCK || DEBUG
            var content = GetFileContent(faileName);
            var set = new KeyValuePair<Broker, string>(currentBroker, content);
            
            var source = BrokerClient.ParseSourceResult<FinhubSourceSet>(set);
            Assert.Equal(OK_RESULT, source.Symbol);
            
            var model = new FinhubSet(source);
            Assert.NotNull(model);
            Assert.True(model.Rows.Count > 0);
#else
            var resultSet = await BrokerClient.GetHistoryAsync(parameters, Enums.Broker.Finnhub);
            foreach (var set in resultSet)
            {
                var source = BrokerClient.ParseSourceResult<FinhubSourceSet>(set);
                Assert.Equal(OK_RESULT, source.Symbol);
                
                var model = new FinhubSet(source);
                Assert.NotNull(model);
                Assert.True(model.Rows.Count > 0);
            }
#endif
        }
    }
}