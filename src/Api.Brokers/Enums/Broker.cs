﻿using Api.Brokers.Attributes;
using Api.Brokers.Models.Finhub;

namespace Api.Brokers.Enums
{
    public enum Broker
    {
        Marketstack = 0,

        [ModelBind(typeof(FinhubSourceSet), typeof(FinhubSet))] 
        Finnhub = 1,
        Barchart = 2,
        IexCloudApi = 3,
        Intrinio = 4,
        Polygon = 5
    }
}
