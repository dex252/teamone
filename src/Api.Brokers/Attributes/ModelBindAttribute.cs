﻿using System;

namespace Api.Brokers.Attributes
{
    internal class ModelBindAttribute : Attribute
    {
        /// <summary>
        /// Модель, возвращаемая со стороны url запроса
        /// </summary>
        public Type SourceType { get; }

        /// <summary>
        /// Модель, полученная в результате обработки source
        /// </summary>
        public Type ModelType { get; }

        public ModelBindAttribute(Type sourceType, Type modelType)
        {
            SourceType = sourceType;
            ModelType = modelType;
        }
    }
}
