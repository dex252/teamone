﻿using Api.Brokers.Facades;

namespace Api.Brokers.Strategy
{
    abstract class BaseStrategy
    {
        protected RestClient RestClient { get; }

        public BaseStrategy(string url)
        {
            RestClient = new RestClient(url);
        }
    }
}
