﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Api.Brokers.Strategy
{
    public interface IStrategy
    {
        Task<string> GetHistoryAsync(Dictionary<string, string> parameters);
    }
}
