﻿using Api.Brokers.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Api.Brokers.Strategy
{
    internal class BrokerContext : IStrategy
    {
        private IStrategy CurrentStrategy { get; set; }

        public async Task<string> GetHistoryAsync(Dictionary<string, string> parameters)
        {
            var history = await CurrentStrategy.GetHistoryAsync(parameters);
            return history;
        }

        internal void SetStrategy(Broker broker)
        {
            switch (broker)
            {
                case Broker.Finnhub:
                    {
                        CurrentStrategy = new FinnhubStrategy();
                        break;
                    }
                case Broker.Barchart:
                case Broker.IexCloudApi:
                case Broker.Intrinio:
                case Broker.Polygon:
                default:
                    throw new NotImplementedException();

            }
        }


    }
}
