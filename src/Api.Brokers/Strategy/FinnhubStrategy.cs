﻿using Api.Brokers.Enums;
using Api.Brokers.Facades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Api.Brokers.Strategy
{
    internal class FinnhubStrategy : BaseStrategy, IStrategy
    {
        const string API_KEY = "chtb5hhr01qu6q7erja0chtb5hhr01qu6q7erjag";
        const string URL = "https://finnhub.io/api/v1/";
        const Broker CurrentBroker = Broker.Finnhub;

        Dictionary<string, string> Headers { get; }

        public FinnhubStrategy() : base (URL)
        {
            Headers = new Dictionary<string, string>
            {
                { "X-Finnhub-Token", API_KEY },
                { "Accept", "application/json" }
            };
        }

        public async Task<string> GetHistoryAsync(Dictionary<string,string> parameters)
        {
            var template = "stock/candle?symbol={0}&resolution={1}&from={2}&to={3}&token={4}";

            var symbol = parameters[$"{CurrentBroker}_SYMBOL"];
            var resolution = parameters[$"{CurrentBroker}_RESOLUTION"];
            var fromDate = parameters[$"{CurrentBroker}_FROM_DATE"];
            var toDate = parameters[$"{CurrentBroker}_TO_DATE"];

            var path = string.Format(template, symbol, resolution, fromDate, toDate, API_KEY);
            
            RestClient.SetHeaders(Headers);
            var result = await RestClient.RequestAsync(path);

            return result;
        }
    }
}
