﻿using Api.Brokers.Enums;
using Api.Brokers.Generator.Strategy;
using Api.Brokers.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Brokers.Generator
{
    public class GeneratorManager
    {
        public int LastDays { get; }
        public bool IsRemoveAnomaly { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lastDays">За сколько последних дней взять срединие значения данных</param>
        /// <param name="isRemoveAnomaly">Следует ли избавиться от сильных отклонений в значениях</param>
        public GeneratorManager(int lastDays, bool isRemoveAnomaly = true)
        {
            if (lastDays < 3) 
            {
                throw new Exception("Недостаточное число входных данных для генерации");
            }

            LastDays = lastDays;
            IsRemoveAnomaly = isRemoveAnomaly;
        }

        public IEnumerable<K> GenerateData<T, K>(T model, Broker broker)
            where K: BaseRow
        {
            switch (broker)
            {
                case Broker.Finnhub:
                    {
                        return new FinhubStrategy()
                            .GenerateData(model, LastDays, IsRemoveAnomaly)
                            .ToList()
                            .ConvertAll(row => row as K);
                    }

                default: throw new NotImplementedException();
            }

        }
    }
}
