﻿using Api.Brokers.Models;
using System.Collections.Generic;

namespace Api.Brokers.Generator.Strategy
{
    public interface IGeneratorStrategy
    {
        IEnumerable<BaseRow> GenerateData<T>(T set, int lastDays, bool isRemoveAnomaly);
    }
}