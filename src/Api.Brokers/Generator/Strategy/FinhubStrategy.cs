﻿using Api.Brokers.Models;
using Api.Brokers.Models.Finhub;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace Api.Brokers.Generator.Strategy
{
    public class FinhubStrategy : IGeneratorStrategy
    {
        public FinhubStrategy()
        {
        }

        public IEnumerable<BaseRow> GenerateData<T>(T set, int lastDays, bool isRemoveAnomaly)
        {
            var finhubSet = set as FinhubSet;

            var sourceRows = finhubSet.Rows.OrderByDescending(x => x.Timestamp)
                                           .Take(lastDays)
                                           .ToList();

            var settings = new Settings(sourceRows);
            return Generate(settings);
        }

        private IEnumerable<BaseRow> Generate(Settings settings)
        {
            var rnd = new Random();
            var generated = new List<BaseRow>(settings.Days);
            for (int i = 0; i < settings.Days; i++)
            {
                #region На_подумать
                //TODO: Обмозговать

                //var previous = (i > 0 ?
                //        generated[i - 1] :
                //        settings.LastSourceRow) as Row;
                //var prePrevious = (i > 1 ?
                //        generated[i - 2] :
                //        i > 0 ?
                //            settings.LastSourceRow :
                //            settings.PreLastSourceRow) as Row;

                //var isLowDirection = prePrevious.CloseCost > previous.CloseCost;
                #endregion

                var row = new Row();
                row.Timestamp = settings.LastTimestamp.AddDays(i + 1);

                SetVolume(settings, rnd, row);
                SetCosts(settings, rnd, row);
                
                generated.Add(row);
            }

            return generated;
        }

        /// <summary>
        /// Установить цены
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="rnd"></param>
        /// <param name="row"></param>
        private void SetCosts(Settings settings, Random rnd, Row row)
        {
            row.MinCost = rnd.NextDouble() * (settings.AverageMinCost - settings.MinMinCost) + settings.MinMinCost;
            row.MaxCost = rnd.NextDouble() * (settings.MaxMaxCost - settings.AverageMaxCost) + settings.AverageMaxCost;

            while (row.MinCost > row.MaxCost)
            {
                row.MinCost = row.MinCost - settings.MinMinCost * 0.1 * rnd.NextDouble();
                row.MaxCost = row.MaxCost + settings.MinMaxCost * 0.1 * rnd.NextDouble();
            }

            SetOpenCloseCosts(rnd, row);
        }

        /// <summary>
        /// Установить цены при открытии и закрытии торгов
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="rnd"></param>
        /// <param name="row"></param>
        private void SetOpenCloseCosts(Random rnd, Row row)
        {
            var range = row.MaxCost - row.MinCost;
            row.CloseCost = rnd.NextDouble() * range + row.MinCost;
            row.OpenCost = rnd.NextDouble() * range + row.MinCost;
        }

        /// <summary>
        /// Количество акций
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="rnd"></param>
        /// <param name="row"></param>
        private void SetVolume(Settings settings, Random rnd, Row row)
        {
            row.Volume = rnd.Next(settings.MinVolume, settings.MaxVolume);

            while (row.Volume > settings.MaxVolume + settings.AverageVolume)
            {
                row.Volume = row.Volume - rnd.Next(1, 5);
            }

            while (row.Volume < settings.MinVolume - settings.AverageVolume)
            {
                row.Volume = row.Volume + rnd.Next(1, 5);
            }
        }
    }
}
