﻿using Api.Brokers.Models.Finhub;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Brokers.Models
{
    public abstract class BaseSet<T>
        where T : BaseRow
    {
        public List<T> Rows { get; set; }

        public BaseSet() { }
    }
}
