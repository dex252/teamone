﻿using System;

namespace Api.Brokers.Models.Finhub
{
    public class Row: BaseRow
    {
        public double CloseCost { get; set; }

        /// <summary>
        /// Список наивысших цен
        /// </summary>
        public double MaxCost { get; set; }

        /// <summary>
        /// Список наименьших цен
        /// </summary>
        public double MinCost { get; set; }

        /// <summary>
        /// Список открытых цен
        /// </summary>
        public double OpenCost { get; set; }


        /// <summary>
        /// Время записи
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Объем активов
        /// </summary>
        public int Volume { get; set; }
    }
}
