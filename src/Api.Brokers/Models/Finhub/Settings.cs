﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Brokers.Models.Finhub
{
    public class Settings
    {
        public double MaxCloseCost { get; set; }
        public double MinCloseCost { get; set; }
        public double AverageCloseCost { get; set; }

        /// <summary>
        /// Список наивысших цен
        /// </summary>
        public double MaxMaxCost { get; set; }
        public double MinMaxCost { get; set; }
        public double AverageMaxCost { get; set; }


        /// <summary>
        /// Список наименьших цен
        /// </summary>
        public double MaxMinCost { get; set; }
        public double MinMinCost { get; set; }
        public double AverageMinCost { get; set; }

        /// <summary>
        /// Список открытых цен
        /// </summary>
        public double MaxOpenCost { get; set; }
        public double MinOpenCost { get; set; }
        public double AverageOpenCost { get; set; }

        /// <summary>
        /// Объем активов
        /// </summary>
        public int MaxVolume { get; set; }
        public int MinVolume { get; set; }
        public int AverageVolume { get; set; }

        /// <summary>
        /// Время записи
        /// </summary>
        public DateTime LastTimestamp { get; set; }
        /// <summary>
        /// Сегодняшний день
        /// </summary>
        public DateTime Now { get; set; }
        /// <summary>
        /// Сколько дней следует сгенерировать
        /// </summary>
        public int Days { get; set; }

        public Row LastSourceRow{get;set;}
        public Row PreLastSourceRow { get; set; }

        public Settings(List<Row> sourceRows)
        {
            var count = sourceRows.Count;
            LastSourceRow = sourceRows[0];
            PreLastSourceRow = sourceRows[1];

            MaxCloseCost = sourceRows.Max(e => e.CloseCost);
            MinCloseCost = sourceRows.Min(e => e.CloseCost);
            AverageCloseCost = sourceRows.Average(e => e.CloseCost);

            MaxMaxCost = sourceRows.Max(e => e.MaxCost);
            MinMaxCost = sourceRows.Min(e => e.MaxCost);
            AverageMaxCost = sourceRows.Average(e => e.MaxCost);

            MaxMinCost = sourceRows.Max(e => e.MinCost);
            MinMinCost = sourceRows.Min(e => e.MinCost);
            AverageMinCost = sourceRows.Average(e => e.MinCost);

            MaxOpenCost = sourceRows.Max(e => e.OpenCost);
            MinOpenCost = sourceRows.Min(e => e.OpenCost);
            AverageOpenCost = sourceRows.Average(e => e.OpenCost);

            MaxVolume = sourceRows.Max(e => e.Volume);
            MinVolume = sourceRows.Min(e => e.Volume);
            AverageVolume = (int)sourceRows.Average(e => e.Volume);

            LastTimestamp = sourceRows.Max(e => e.Timestamp);
            Now = DateTime.Now;
            Days = (Now - LastTimestamp).Days;
        }
    }
}
