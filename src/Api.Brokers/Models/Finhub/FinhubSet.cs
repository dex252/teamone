﻿using System;
using System.Collections.Generic;
using Api.Brokers.Extensions;

namespace Api.Brokers.Models.Finhub
{
    public class FinhubSet: BaseSet<Row>
    {
        public FinhubSet() { }

        public FinhubSet(FinhubSourceSet value)
        {
            var count = value.Volume.Count;
            Rows = new List<Row>(count);
            for (int index = 0; index < count; index++) 
            {
                var row = new Row();
                row.CloseCost = value.CloseCost[index];
                row.MaxCost = value.MaxCost[index];
                row.MinCost = value.MinCost[index];
                row.OpenCost = value.OpenCost[index];
                row.Timestamp = value.Timestamps[index].FromUnixTime();
                row.Volume = value.Volume[index];
                
                Rows.Add(row);
            }
        }
    }
}
