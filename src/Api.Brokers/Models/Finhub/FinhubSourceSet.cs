﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Brokers.Models.Finhub
{
    public class FinhubSourceSet
    {
        /// <summary>
        /// Список закрытых цен
        /// </summary>
        [JsonProperty("c")]
        public List<double> CloseCost { get; set; }

        /// <summary>
        /// Список наивысших цен
        /// </summary>
        [JsonProperty("h")]
        public List<double> MaxCost { get; set; }

        /// <summary>
        /// Список наименьших цен
        /// </summary>
        [JsonProperty("l")]
        public List<double> MinCost { get; set; }

        /// <summary>
        /// Список открытых цен
        /// </summary>
        [JsonProperty("o")]
        public List<double> OpenCost { get; set; }

        /// <summary>
        /// Статус ответа
        /// </summary>
        [JsonProperty("s")]
        public string Symbol { get; set; }

        /// <summary>
        /// Время записи
        /// </summary>
        [JsonProperty("t")]
        public List<long> Timestamps { get; set; }

        /// <summary>
        /// Объем активов
        /// </summary>
        [JsonProperty("v")]
        public List<int> Volume { get; set; }
    }
}
