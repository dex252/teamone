﻿using System;

namespace Api.Brokers.Extensions
{
    public static class DateTimeExtension
    {
        public static long ToUnixTime(this DateTime date) => (long)date.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        public static DateTime FromUnixTime(this long unixTime) => DateTimeOffset.FromUnixTimeSeconds(unixTime).LocalDateTime;
    }
}
