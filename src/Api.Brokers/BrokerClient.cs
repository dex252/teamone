﻿using Api.Brokers.Enums;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Brokers
{
    public class BrokerClient
    {
        private BrokerParser BrokerParser { get; }

        public BrokerClient()
        {
            BrokerParser = new BrokerParser();
        }

        public async Task<ConcurrentDictionary<Broker, string>> GetHistoryAsync(Dictionary<string, string> parameters, params Broker[] brokers)
        {
            var context = new BrokerManager(brokers);
            var history = await context.GetHistoryAsync(parameters);
            return history;
        }

        public T ParseSourceResult<T>(KeyValuePair<Broker, string> set) => BrokerParser.ParseSourceResult<T>(set);
    }
}
