﻿using Api.Brokers.Enums;
using Api.Brokers.Strategy;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Brokers
{
    internal class BrokerManager
    {
        private Enums.Broker[] Brokers { get; }
        public BrokerManager(Enums.Broker[] brokers)
        {
            Brokers = brokers;
        }

        internal async Task<ConcurrentDictionary<Broker, string>> GetHistoryAsync(Dictionary<string, string> parameters)
        {
            var resultSet = new ConcurrentDictionary<Broker, string>();
            var context = new BrokerContext();
            foreach (var broker in Brokers)
            {
                context.SetStrategy(broker);
                var history = await context.GetHistoryAsync(parameters);
                resultSet.AddOrUpdate(broker, history, (key, oldValue) => history);
            }

            return resultSet;
        }
    }
}
