﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Api.Brokers.Facades
{
    internal class RestClient : HttpClient
    {
        private Uri Url;
        public RestClient(string url)
        {
            Url = new Uri(url);
        }

        internal async Task<string> RequestAsync(string path)
        {
            var url = new Uri(Url, path);
            var response = await GetAsync(url);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"{response.StatusCode} {response.Content.ReadAsStringAsync()}");
            }

            return await response.Content.ReadAsStringAsync();
        }

        internal void SetHeaders(Dictionary<string, string> headers)
        {
            DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            foreach (var header in headers)
            {
                if (!DefaultRequestHeaders.Contains(header.Key))
                {
                    DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
        }
    }
}
