﻿using Api.Brokers.Attributes;
using Api.Brokers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Api.Brokers
{
    public class BrokerParser
    {
        public int ModelBind { get; private set; }

        public T ParseSourceResult<T>(KeyValuePair<Broker, string> set)
        {
            var isValid = IsValidSourceType(set.Key, typeof(T));
            if (!isValid)
            {
                throw new Exception($"Брокер {set.Key} не привязан к указанной модели данных");
            }

            var content = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(set.Value);
            return content;
        }

        private bool IsValidSourceType(Broker key, Type type)
        {
            var bindType = key.GetType()?.
                GetMember(key.ToString())?.
                FirstOrDefault()?.
                GetCustomAttribute<ModelBindAttribute>()?.SourceType;

            if (bindType != type)
                return false;

            return true;
        }
    }
}
